var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('bootflat.scss', 'public/build-css').version('public/build-css');
});

//PNotify
//https://sciactive.com/pnotify/
elixir(function (mix) {
    mix.copy('node_modules/pnotify/dist/pnotify.css', 'public/build-css/pnotify.css');
    mix.copy('node_modules/pnotify/dist/pnotify.buttons.css', 'public/build-css/pnotify.buttons.css');
    mix.copy('node_modules/pnotify/dist/pnotify.mobile.css', 'public/build-css/pnotify.mobile.css');

    mix.copy('node_modules/pnotify/dist/pnotify.js', 'public/build-js/pnotify.js');
    mix.copy('node_modules/pnotify/dist/pnotify.buttons.js', 'public/build-js/pnotify.buttons.js');
    mix.copy('node_modules/pnotify/dist/pnotify.mobile.js', 'public/build-js/pnotify.mobile.js');
    mix.copy('node_modules/pnotify/dist/pnotify.animate.js', 'public/build-js/pnotify.animate.js');

    mix.copy('node_modules/js-cookie/src/js.cookie.js', 'public/build-js/js.cookie.js');
});
