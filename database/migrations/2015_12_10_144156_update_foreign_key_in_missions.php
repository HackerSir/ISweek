<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForeignKeyInMissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('missions', function (Blueprint $table) {
            $table->integer('first_page_id')->unsigned()->nullable()->change();
            $table->integer('key_button_id')->unsigned()->nullable()->change();
        });
        DB::table('missions')->where('first_page_id', '=', 0)->update(['first_page_id' => null]);
        DB::table('missions')->where('key_button_id', '=', 0)->update(['key_button_id' => null]);
        Schema::table('missions', function (Blueprint $table) {
            $table->foreign('first_page_id')->references('id')->on('pages')
                ->onUpdate('cascade')->onDelete('set null');
            $table->foreign('key_button_id')->references('id')->on('buttons')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('missions', function (Blueprint $table) {
            $table->dropForeign('missions_first_page_id_foreign');
            $table->dropForeign('missions_key_button_id_foreign');
            $table->integer('first_page_id')->unsigned()->change();
            $table->integer('key_button_id')->unsigned()->change();
        });
    }
}
