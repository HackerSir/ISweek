<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveIpFromClickRecordsToRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('click_records', function (Blueprint $table) {
            $table->dropColumn('ip');
        });
        Schema::table('records', function (Blueprint $table) {
            $table->string('ip', 40)->after('mission_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('click_records', function (Blueprint $table) {
            $table->string('ip', 40)->after('button_id');
        });
        Schema::table('records', function (Blueprint $table) {
            $table->dropColumn('ip');
        });
    }
}
