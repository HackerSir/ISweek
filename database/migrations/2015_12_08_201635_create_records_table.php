<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('player_nid');
            $table->integer('mission_id')->unsigned();
            $table->timestamps();

            $table->foreign('player_nid')->references('nid')->on('players')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('mission_id')->references('id')->on('missions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('records');
    }
}
