<?php

use App\Setting;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStoryBackgroundSettingType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $storyBackground = Setting::find('story-background');
        $storyBackground->type = 'multiline';
        $storyBackground->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $storyBackground = Setting::find('story-background');
        $storyBackground->type = 'markdown';
        $storyBackground->save();
    }
}
