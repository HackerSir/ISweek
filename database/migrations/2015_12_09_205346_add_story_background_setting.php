<?php

use App\Setting;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoryBackgroundSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Setting::create([
            'id' => 'story-background',
            'type' => 'markdown',
            'desc' => '故事背景(會在進入遊戲後播放)'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Setting::find('story-background')->delete();
    }
}
