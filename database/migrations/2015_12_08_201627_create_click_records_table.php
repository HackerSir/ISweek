<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClickRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('click_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('player_nid');
            $table->integer('page_id')->unsigned();
            $table->integer('button_id')->unsigned();
            $table->string('ip', 40);
            $table->timestamps();

            $table->foreign('player_nid')->references('nid')->on('players')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('page_id')->references('id')->on('pages')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('button_id')->references('id')->on('buttons')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('click_records');
    }
}
