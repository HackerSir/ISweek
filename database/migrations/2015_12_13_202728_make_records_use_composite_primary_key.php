<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeRecordsUseCompositePrimaryKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('records', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->primary(['player_nid', 'mission_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `records` DROP PRIMARY KEY;');
        Schema::table('records', function (Blueprint $table) {
            $table->bigInteger('id', true, true)->first();
        });
    }
}
