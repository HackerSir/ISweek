<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'missions';

    public function up()
    {
        DB::statement("ALTER TABLE `$this->table` MODIFY `open_at` TIMESTAMP NULL;");
        DB::statement("ALTER TABLE `$this->table` MODIFY `end_at` TIMESTAMP NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('missions', function (Blueprint $table) {
            DB::statement("ALTER TABLE `$this->table` MODIFY `open_at` TIMESTAMP NOT NULL;");
            DB::statement("ALTER TABLE `$this->table` MODIFY `end_at` TIMESTAMP NOT NULL;");
        });
    }
}
