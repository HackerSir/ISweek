<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Mission::class, function (Faker\Generator $faker) {
    return [
        'title'   => $faker->sentence,
    ];
});

$factory->define(App\Page::class, function (Faker\Generator $faker) {
    return [
        'info' => StoryBackgroundSeeder::getInfo($faker)
    ];
});

$factory->define(App\Button::class, function (Faker\Generator $faker) {
    return [
        'text' => $faker->word
    ];
});
