<?php

use Illuminate\Database\Seeder;

class StoryBackgroundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //產生故事背景
        $faker = Faker\Factory::create('zh_TW');

        DB::table('settings')
            ->where('id', 'story-background')
            ->update([
                'data' => StoryBackgroundSeeder::getInfo($faker)
            ]);
    }

    static function getInfo(Faker\Generator $faker) {
        return $faker->realText(10) . '^pause' . $faker->realText(50) . '^pause' . $faker->realText(30);
    }
}
