<?php

use App\Mission;
use Illuminate\Database\Seeder;

class MissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $coordinates = [
            ['x' => 975, 'y' => 176],
            ['x' => 911, 'y' => 287],
            ['x' => 818, 'y' => 167],
            ['x' => 997, 'y' => 370],
            ['x' => 1148, 'y' => 303]
        ];

        factory('App\Mission', 5)->create()
            ->each(function ($m, $index) use ($faker, $coordinates) {
                $m->open_at = Carbon::now()->addDay($index - 2);
                $m->end_at = Carbon::now()->addDay(5);
                $m->x = $coordinates[$index]['x'];
                $m->y = $coordinates[$index]['y'];

                $pages = [];
                $lastButtonId = 0;
                for ($i = 0; $i < 5; $i++) {
                    $page = factory(App\Page::class)->make();
                    $m->pages()->save($page);
                    $page->info = $page->id . ': ' . $page->info;
                    $page->save();
                    array_push($pages, $page);

                    $button = factory(App\Button::class)->make();
                    $page->buttons()->save($button);
                    $lastButtonId = $button->id;
                }
                for ($i = 0; $i < 5 - 1; $i++) {
                    $buttons = $pages[$i]->buttons;
                    foreach ($buttons as $button) {
                        $button->target_page_id = $pages[$i + 1]->id;
                        $button->save();
                    }
                }
                $m->first_page_id = $m->pages()->first()->id;
                $m->key_button_id = $lastButtonId;
                $m->save();
            });
    }
}
