<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    protected $table = 'missions';
    protected $fillable = [
        'title',
        'open_at',
        'end_at',
        'first_page_id',
        'key_button_id',
        'require_mission_id',
        'require',
        'x',
        'y'
    ];

    public function pages()
    {
        return $this->hasMany('App\Page')->orderBy('order');
    }

    public function inStatsPages()
    {
        return $this->hasMany('App\Page')->where('in_stats', '=', true)->orderBy('order');
    }

    public function records()
    {
        return $this->hasMany('App\Record');
    }

    public function firstPage()
    {
        return $this->hasOne('App\Page', 'id', 'first_page_id');
    }

    public function keyButton()
    {
        return $this->hasOne('App\Button', 'id', 'key_button_id');
    }

    public function requireMission()
    {
        return $this->hasOne('App\Mission', 'id', 'require_mission_id');
    }

    public function getMissionSelectionsAttribute()
    {
        $missionSelections = [null => '請選擇任務'];
        foreach (Mission::all() as $mission) {
            $missionSelections[$mission->id] = $mission->title;
        }
        return $missionSelections;
    }

    public function getPageSelectionsAttribute()
    {
        $pageSelections = [null => '請選擇頁面'];
        foreach ($this->pages as $page) {
            $pageSelections[$page->id] = $page->abstract;
        }
        return $pageSelections;
    }

    public function getButtonSelectionsAttribute()
    {
        $buttonSelections = [null => '請選擇按鈕'];
        foreach ($this->pages as $page) {
            foreach ($page->buttons as $button) {
                $buttonSelections[$page->abstract][$button->id] = $button->text;
            }
        }
        return $buttonSelections;
    }

    public function getCanPlayAttribute()
    {
        if (empty($this->open_at)) {
            return false;
        }
        $openAt = new Carbon($this->open_at);
        if ($openAt->lte(Carbon::now())) {
            return true;
        }
        return false;
    }
}
