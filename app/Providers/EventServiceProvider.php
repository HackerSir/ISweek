<?php

namespace App\Providers;

use Exception;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Log\Writer;
use Monolog\Logger;
use Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        'App\Events\SomeEvent' => [
//            'App\Listeners\EventListener',
//        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //紀錄郵件發送時間與資訊
        $events->listen('mailer.sending', function ($message) {
            try {
                $logger = new Writer(new Logger('Mail-log'));
                $logger->useDailyFiles(storage_path('mail-log/mail.log'), 365);

                $logger->info(
                    'From: '.$this->getMailAddressString($message->getFrom()).
                    ',To: '.$this->getMailAddressString($message->getTo()).
                    ',Subject: '.$message->getSubject()
                );
            } catch (Exception $e) {
                Log::error($e);
            }
        });
    }

    private function getMailAddressString($addressArray) {
        $result = '';
        foreach ($addressArray as $address => $name) {
            $result .= '<'.$name.'>'.$address.' ';
        }
        return $result;
    }
}
