<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';
    protected $fillable = ['name', 'amount'];

    public function players()
    {
        return $this->hasMany('App\Player');
    }

    public function getLeftAttribute()
    {
        return $this->amount - $this->players->count();
    }

}
