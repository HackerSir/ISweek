<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Player extends Model
{
    protected $table = 'players';
    protected $fillable = ['nid', 'get_price_at', 'price_id'];
    protected $primaryKey = 'nid';

    public function clickRecords()
    {
        return $this->hasMany('App\ClickRecord', 'player_nid', 'nid');
    }

    public function records()
    {
        return $this->hasMany('App\Record', 'player_nid', 'nid');
    }

    public function requireRecords()
    {
        return $this->hasMany('App\Record', 'player_nid', 'nid')->whereHas('mission', function ($query) {
            $query->where('require', '=', true);
        });
    }

    public function validRecords()
    {
        return $this->hasMany('App\Record', 'player_nid', 'nid')->whereHas('mission', function ($query) {
            $query->whereNull('missions.end_at')
                ->orWhereRaw('records.created_at <= missions.end_at');
        });
    }

    public function validRequireRecords()
    {
        return $this->hasMany('App\Record', 'player_nid', 'nid')->whereHas('mission', function ($query) {
            $query->where('require', '=', true)
                ->where(function ($query) {
                    $query->whereNull('missions.end_at')
                        ->orWhereRaw('records.created_at <= missions.end_at');
                });
        });
    }

    public function price()
    {
        return $this->belongsTo('App\Price');
    }

    public function getFinishMissionCountAttribute()
    {
        $records = $this->validRecords;
        $requireMissionIds = Mission::where('require', '=', true)->lists('id')->toArray();
        $count = 0;
        foreach ($records as $record) {
            if (in_array($record->mission_id, $requireMissionIds)) {
                $count++;
            }
        }
        return $count;
    }

    public function getFinishAllMissionsAttribute()
    {
        $requireMissionCount = Mission::where('require', '=', true)->count();
        if ($this->FinishMissionCount >= $requireMissionCount) {
            return true;
        }
        return false;
    }

    public function getCanGetPriceAttribute()
    {
        if ($this->get_price_at) {
            return false;
        }
        if (!$this->finishAllMissions) {
            return false;
        }
        return true;
    }

    public function hasFinishMission(Mission $mission = null)
    {
        if (!$mission) {
            return false;
        }
        $recordCount = Record::where('player_nid', '=', $this->nid)->where('mission_id', '=', $mission->id)->count();
        if (!$recordCount) {
            return false;
        }
        return true;
    }

    public function canPlayMission(Mission $mission = null)
    {
        //任務不存在
        if (!$mission) {
            return false;
        }
        //任務未開放
        if (!$mission->canPlay) {
            return false;
        }
        //有前置任務
        if ($mission->requireMission) {
            //前置任務未完成
            if (!$this->hasFinishMission($mission->requireMission)) {
                return false;
            }
        }
        return true;
    }
}
