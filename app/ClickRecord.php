<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClickRecord extends Model
{
    protected $table = 'click_records';
    protected $fillable = ['player_nid', 'button_id'];

    public function player()
    {
        return $this->belongsTo('App\Player', 'player_nid');
    }

    public function button()
    {
        return $this->belongsTo('App\Button');
    }

}
