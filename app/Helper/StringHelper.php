<?php

namespace App\Helper;

class StringHelper
{

    /**
     * 擷取字串摘要
     * 來源：http://codex.wiki/post/132691-839
     *
     * @param string $string 字串
     * @param int $length 擷取長度
     * @param string $dot 結尾符
     * @param string $charset 編碼
     * @return mixed|string
     */
    public static function cutstr($string, $length, $dot = '', $charset = 'utf-8')
    {
        if (strlen($string) <= $length) {
            return $string;
        }
        $pre = chr(1);
        $end = chr(1);
        //保護特殊字元串
        $string = str_replace(
            ['&', '"', '<', '>'],
            [$pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end],
            $string
        );
        $strcut = '';
        if (strtolower($charset) == 'utf-8') {
            $n = $tn = $noc = 0;
            while ($n < strlen($string)) {
                $t = ord($string[$n]);
                if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                    $tn = 1;
                    $n++;
                    $noc++;
                } elseif (194 <= $t && $t <= 223) {
                    $tn = 2;
                    $n += 2;
                    $noc += 2;
                } elseif (224 <= $t && $t <= 239) {
                    $tn = 3;
                    $n += 3;
                    $noc += 2;
                } elseif (240 <= $t && $t <= 247) {
                    $tn = 4;
                    $n += 4;
                    $noc += 2;
                } elseif (248 <= $t && $t <= 251) {
                    $tn = 5;
                    $n += 5;
                    $noc += 2;
                } elseif ($t == 252 || $t == 253) {
                    $tn = 6;
                    $n += 6;
                    $noc += 2;
                } else {
                    $n++;
                }
                if ($noc >= $length) {
                    break;
                }
            }
            if ($noc > $length) {
                $n -= $tn;
            }
            $strcut = substr($string, 0, $n);
        } else {
            for ($i = 0; $i < $length; $i++) {
                $strcut .= ord($string[$i]) > 127 ? $string[$i] . $string[++$i] : $string[$i];
            }
        }
        //還原特殊字元串
        $strcut = str_replace(
            [$pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end],
            ['&', '"', '<', '>'],
            $strcut
        );
        //修復出現特殊字元串截段的問題
        $pos = strrpos($strcut, chr(1));
        if ($pos !== false) {
            $strcut = substr($strcut, 0, $pos);
        }
        return $strcut . $dot;
    }

    /**
     * 將文字自動換行
     *
     * @param string $string 字串
     * @param bool|false $htmlSupport 是否支援HTML
     * @return string
     */
    public static function nl2br($string, $htmlSupport = false)
    {
        if (!$htmlSupport) {
            $string = htmlspecialchars($string);
        }
        return nl2br($string);
    }

    public static function isNID($string)
    {
        if (empty($string)) {
            return false;
        }
        $pattern = "/^(([depmv]([0-9]){7})|(t[0-9]{5}))$/i";
        if (preg_match($pattern, $string)) {
            return true;
        }
        return false;
    }
}
