<?php

namespace App\Http\Controllers;

use App\Button;
use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ButtonController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //限管理員
        $this->middleware('role:admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = Page::find(Input::get('page_id'));
        if (!$page) {
            return redirect()->route('mission.index')
                ->with('warning', '頁面不存在');
        }
        return view('button.createOrEdit')->with('page', $page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = Page::find(Input::get('page_id'));
        if (!$page) {
            return redirect()->route('mission.index')
                ->with('warning', '頁面不存在');
        }

        $validator = Validator::make($request->all(), [
            'text' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('button.create', ['page_id' => $page->id])
                ->withErrors($validator)
                ->withInput();
        }
        $order = ($page->buttons->count() > 0) ? $page->buttons->max('order') + 1 : 0;
        $button = Button::create([
            'page_id' => $page->id,
            'text' => $request->get('text'),
            'target_page_id' => $request->has('target_page_id') ? $request->get('target_page_id') : null,
            'order' => $order
        ]);
        //過關按鈕
        $isKeyButton = $request->has('is_key_button');
        if ($isKeyButton) {
            $mission = $button->page->mission;
            $mission->key_button_id = $button->id;
            $mission->save();
        }

        return redirect()->route('mission.show', $page->mission->id)
            ->with('global', '按鈕已新增');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $button = Button::find($id);
        if (!$button) {
            return redirect()->route('mission.index')
                ->with('warning', '按鈕不存在');
        }
        return view('button.createOrEdit')->with('button', $button)->with('page', $button->page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $button = Button::find($id);
        if (!$button) {
            return redirect()->route('mission.index')
                ->with('warning', '按鈕不存在');
        }

        $validator = Validator::make($request->all(), [
            'text' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('button.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $button->text = $request->get('text');
        $button->target_page_id = $request->has('target_page_id') ? $request->get('target_page_id') : null;
        $button->save();
        //過關按鈕
        $isKeyButton = $request->has('is_key_button');
        $mission = $button->page->mission;
        if ($isKeyButton) {
            $mission->key_button_id = $button->id;
            $mission->save();
        } elseif ($mission->key_button_id == $button->id) {
            $mission->key_button_id = null;
            $mission->save();
        }

        return redirect()->route('mission.show', $button->page->mission->id)
            ->with('global', '按鈕已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $button = Button::find($id);
        if (!$button) {
            return redirect()->route('mission.index')
                ->with('warning', '按鈕不存在');
        }
        $mission = $button->page->mission;
        $button->delete();
        return redirect()->route('mission.show', $mission->id)
            ->with('global', '按鈕已刪除');
    }

    public function sort(Request $request)
    {
        //只接受Ajax請求
        if (!$request->ajax()) {
            return 'error';
        }
        $page = Page::find($request->get('pageId'));
        if (!$page) {
            return '頁面不存在';
        }
        //若無按鈕，直接回傳成功（理論上不會發生）
        if ($page->buttons->count() == 0) {
            return 'success';
        }
        //取得新的id清單
        $idList = $request->get('idList');
        foreach ($idList as $order => $id) {
            $button = Button::find($id);
            $button->order = $order;
            $button->save();
        }
        return 'success';
    }
}
