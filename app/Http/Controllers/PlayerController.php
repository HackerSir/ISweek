<?php

namespace App\Http\Controllers;

use App\Mission;
use App\Player;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class PlayerController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //限管理員
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amountPerPage = 100;
        //搜尋
        $playerQuery = Player::with('records', 'requireRecords', 'validRecords', 'validRequireRecords')
            ->orderBy('created_at');
        if (Input::has('q')) {
            $q = Input::get('q');
            //模糊匹配
            $q = '%' . $q . '%';
            //搜尋：NID
            $playerQuery->where('nid', 'like', $q);
        }
        if (Input::has('min')) {
            $min = Input::get('min');
            $playerQuery->has('records', '>=', $min);
        }
        if (Input::has('max')) {
            $max = Input::get('max');
            $playerQuery->has('records', '<=', $max);
        }
        $totalCount = $playerQuery->count();
        $players = $playerQuery->paginate($amountPerPage);
        $missions = Mission::with('records.player')->get();
        $records = [];
        foreach ($missions as $mission) {
            $records[$mission->id] = [];
            foreach ($mission->records as $record) {
                $records[$mission->id][$record->player->nid] = [
                    'time' => $record->created_at,
                    'ip' => $record->ip,
                    'valid' => $record->isValid
                ];
            }
        }
        return view('player.list')
            ->with('players', $players)
            ->with('amountPerPage', $amountPerPage)
            ->with('totalCount', $totalCount)
            ->with('missions', $missions)
            ->with('records', $records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export()
    {
        $time = Carbon::now()->format('Ymd_His');
        Excel::create('Export_' . $time, function ($excel) {
            $excel->sheet('玩家名單', function ($sheet) {
                //凍結第一列
                $sheet->freezeFirstRow();
                //自動篩選
                $sheet->setAutoFilter('A1:D1');
                //設定寬度
                $sheet->setWidth([
                    'A' => 20,
                    'B' => 15,
                    'C' => 30,
                    'D' => 30
                ]);
                //型態
                $sheet->setColumnFormat([
                    'A' => '@'
                ]);
                //標題列
                $sheet->rows([
                    [
                        'NID', '完成關卡數', '領獎時間', '領獎項目'
                    ]
                ]);
                //資料
                $players = Player::with('validRecords')->orderBy('created_at')->get();
                foreach ($players as $player) {
                    $sheet->rows([
                        [
                            $player->nid,
                            count($player->records),
                            $player->get_price_at,
                            ($player->price) ? $player->price->name : null
                        ]
                    ]);
                }
            });
        })->download('xlsx');
    }
}
