<?php

namespace App\Http\Controllers;

use App\Helper\StringHelper;
use App\Mission;
use App\Player;
use App\Record;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    //首頁
    public function index()
    {
        //TODO: 可以從資料庫抓取，目前是寫死
        $startTime = Carbon::create(2015, 12, 18, 0, 0, 0);
        $isStart = false;
        if (Carbon::now() >= $startTime) {
            $isStart = true;
        }

        //如果時間到，對全部人顯示 index (遊戲畫面)
        if ($isStart || \Entrust::hasRole('admin')) {
            return view('index')
                ->with('startTime', $startTime)
                ->with('endTime', Carbon::create(2015, 12, 23, 0, 0, 0));
        }
        return view('countdown')->with('startTime', $startTime);
    }

    //活動辦法
    public function activityRule()
    {
        return view('home.activity-rule');
    }

    //前線戰況
    public function missionStatus()
    {
        return view('home.mission-status')
            ->with('missionDataTable', $this->getMissionStatus())
            ->with('missionStats', $this->getMissionStats());
    }

    //隱私權
    public function privacy()
    {
        return view('home.policies')->with('tab', 'privacy');
    }

    //服務條款
    public function terms()
    {
        return view('home.policies')->with('tab', 'terms');
    }

    //常見問題
    public function FAQ()
    {
        return view('home.policies')->with('tab', 'FAQ');
    }

    //工作人員清單
    public function staff()
    {
        return view('home.staff');
    }

    public function getMissionStatus()
    {
        $missions = Mission::with('records')->get();
        $startOfToday = Carbon::now()->startOfDay();

        $missionDataTable = \Lava::DataTable();

        $missionDataTable->addStringColumn('任務名稱')
            ->addNumberColumn('勇者')
            ->addNumberColumn('新勇者¹');
        $cacheMinute = 60 * 24;
        $oldPlayerCount = Cache::remember(
            'oldPlayerCount_' . $startOfToday->toDateTimeString(),
            $cacheMinute,
            function () use ($missions, $startOfToday) {
                $newPlayerCount = [];
                $newPlayerCount['total'] = Player::where('created_at', '<', $startOfToday->toDateTimeString())->count();
                $newPlayerCount['missions'] = [];
                foreach ($missions as $mission) {
                    $newPlayerCount['missions'][$mission->id] = Record::where('mission_id', '=', $mission->id)
                        ->where('created_at', '<', $startOfToday->toDateTimeString())->count();
                }
                return $newPlayerCount;
            }
        );
        foreach ($missions as $mission) {
            $oldCount = isset($oldPlayerCount['missions'][$mission->id])
                ? $oldPlayerCount['missions'][$mission->id] : 0;
            $todayCount = $mission->records->count() - $oldCount;
            $missionDataTable->addRow([
                $mission->title . (($mission->require) ? '' : ' ²'),
                $oldCount,
                $todayCount
            ]);
        }
        //總人數
        $oldPlayer = isset($oldPlayerCount['total']) ? $oldPlayerCount['total'] : 0;
        $todayPlayer = Player::count() - $oldPlayer;
        $missionDataTable->addRow([
            '總人數',
            $oldPlayer,
            $todayPlayer
        ]);

        return $missionDataTable;
    }

    public function getMissionStats()
    {
        $now = Carbon::now();

        $missions = Mission::with('inStatsPages')->get();
        $missionStats = [];
        foreach ($missions as $mission) {
            $statsPages = $mission->inStatsPages;
            if ($statsPages->isEmpty()) {
                continue;
            }

            $openAt = new Carbon($mission->open_at);
            $missionStat = [
                'title' => $mission->title,
                'isStart' => $openAt->lte($now)
            ];

            $pageStats = [];
            if ($missionStat['isStart']) {
                foreach ($statsPages as $page) {
                    $stats = $page->getStatsAttribute();

                    $data = \Lava::DataTable();

                    $data->addStringColumn('按鈕')
                        ->addNumberColumn('人數');

                    $totalCount = 0;
                    foreach ($stats['stats'] as $button) {
                        $data->addRow([$button['text'], $button['count']]);
                        $totalCount += $button['count'];
                    }

                    $pageStats[] = [
                        'info' => StringHelper::nl2br($page->info, true),
                        'time' => $stats['time'],
                        'stats' => $data,
                        'isNoData' => ($totalCount == 0)
                    ];
                }
            }
            $missionStat['pages'] = $pageStats;

            $missionStats[] = $missionStat;
        }
        return $missionStats;
    }
}
