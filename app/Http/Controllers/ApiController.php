<?php

namespace App\Http\Controllers;

use App\Button;
use App\ClickRecord;
use App\Helper\LogHelper;
use App\Mission;
use App\Page;
use App\Player;
use App\Record;
use App\Setting;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ApiController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //Ajax限定
        $this->middleware('ajax');
    }

    public function postIndex(Request $request)
    {
        //取得NID
        $nid = $request->get('nid');
        //移除空格
        $nid = preg_replace("/\\s/", '', $nid);
        if (empty($nid)) {
            return \Response::json(['error' => '沒有輸入學號']);
        }
        if (mb_strlen($nid) > 20) {
            return \Response::json(['error' => '證號輸入有誤']);
        }
        //取得或建立玩家
        $player = Player::find($nid);
        if (!$player) {
            $player = Player::create([
                'nid' => $nid
            ]);
        }
        //記錄NID
        Session::set('nid', $nid);
        //已完成任務ID清單
        $finishedMissionsIds = $player->records->lists('mission_id')->toArray();
        //任務清單
        $missions = Mission::all();
        $missionArray = [];
        foreach ($missions as $mission) {
            $enable = false;
            if (Auth::check() && Auth::user()->inDebugMode) {
                $enable = true;
            } else {
                $enable = $player->canPlayMission($mission);
            }

            $missionArray[$mission->id] = [
                'title' => $mission->title,
                'enable' => $enable,
                'clear' => (in_array($mission->id, $finishedMissionsIds)),
                'open_at' => $mission->open_at,
                'require_mission_id' => $mission->require_mission_id,
                'require' => (bool)$mission->require,
                'x' => $mission->x,
                'y' => $mission->y,
            ];
        }
        //故事
        $story = Setting::getRaw('story-background');
        $json = [
            'story' => $story,
            'missions' => $missionArray
        ];
        return \Response::json($json);
    }

    public function postMission(Request $request, $missionId = null)
    {
        $player = $this->player();
        if (!$player) {
            return \Response::json(['error' => '玩家未登入']);
        }
        $mission = Mission::with('pages.buttons')->find($missionId);
        if (!$mission) {
            return \Response::json(['error' => '任務不存在']);
        }
        if (Auth::check() && Auth::user()->inDebugMode) {
            //管理員可以不受限制
        } else {
            if (!$mission->canPlay) {
                return \Response::json(['error' => '任務未開放']);
            }
            if (!$player->canPlayMission($mission)) {
                return \Response::json(['error' => '玩家不符合此任務資格']);
            }
        }

        $pages = [];
        foreach ($mission->pages as $page) {
            $pages[$page->id] = $this->getPagesData($page);
        }
        $mToken = str_random(32);
        Session::put('mToken_' . $mission->id, $mToken);
        $json = [
            'pages' => $pages,
            'pages_count' => count($pages),
            'first_page_id' => $mission->first_page_id,
            'key_button_id' => $mission->key_button_id,
            'm_token' => $mToken
        ];
        return \Response::json($json);
    }

    public function postPage(Request $request, $pageId = null)
    {
        $player = $this->player();
        if (!$player) {
            return \Response::json(['error' => '玩家未登入']);
        }
        $page = Page::with('buttons')->find($pageId);
        if (!$page) {
            return \Response::json(['error' => '頁面不存在']);
        }
        $json = $this->getPagesData($page);
        return \Response::json($json);
    }

    public function postButton(Request $request, $buttonId = null)
    {
        $player = $this->player();
        if (!$player) {
            return \Response::json(['error' => '玩家未登入']);
        }
        $button = Button::find($buttonId);
        if (!$button) {
            return \Response::json(['error' => '按鈕不存在']);
        }
        if (empty($button->targetPageIdNum)) {
            return \Response::json(['error' => '按鈕無目標頁面']);
        }
        return $this->postPage($request, $button->targetPageIdNum);
    }

    public function postResult(Request $request, $missionId = null)
    {
        $player = $this->player();
        if (!$player) {
            return \Response::json(['error' => '玩家未登入']);
        }
        //取得NID
        $nid = $request->get('nid');
        //移除空格
        $nid = preg_replace("/\\s/", '', $nid);
        if (empty($nid)) {
            return \Response::json(['error' => '未指定NID']);
        }
        if (strcasecmp($player->nid, $nid) != 0) {
            return \Response::json(['error' => 'NID不符']);
        }
        $mission = Mission::with('pages.buttons')->find($missionId);
        if (!$mission) {
            return \Response::json(['error' => '任務不存在']);
        }
        //檢查token
        $mToken = Session::get('mToken_' . $mission->id);
        if (empty($mToken) || !$request->has('m_token') || $mToken != $request->get('m_token')) {
            return \Response::json(['error' => 'Token Mismatch']);
        }
        Session::forget('mToken_' . $mission->id);
        //取得按鈕記錄
        $clickButtonIds = $request->get('record');
        if (empty($clickButtonIds) || !is_array($clickButtonIds)) {
            return \Response::json(['error' => '未包含記錄']);
        }
        //檢查是否所有按鈕都屬於該任務
        foreach ($clickButtonIds as $clickButtonId) {
            $button = Button::find($clickButtonId);
            if (!$button) {
                return \Response::json(['error' => '部分按鈕不存在']);
            }
            if (!$button->page || !$button->page->mission || $button->page->mission->id != $mission->id) {
                return \Response::json(['error' => '部分按鈕無效']);
            }
        }
        //檢查是否已有完成記錄
        $recordCount = Record::where('player_nid', '=', $player->nid)->where('mission_id', '=', $mission->id)->count();
        if ($recordCount) {
            $json = [
                'success' => true,
                'update' => false
            ];
            return \Response::json($json);
        }
        //除錯模式開啟時，不記錄任務進度
        if (Auth::check() && Auth::user()->inDebugMode) {
            $json = [
                'success' => true,
                'update' => false
            ];
            return \Response::json($json);
        }
        //記錄原本是否已完成
        $tempFinishAll = $player->finishAllMissions;
        //寫入記錄
        foreach ($clickButtonIds as $clickButtonId) {
            $clickRecord = ClickRecord::create([
                'player_nid' => $player->nid,
                'button_id' => $clickButtonId
            ]);
        }
        $record = Record::create([
            'player_nid' => $player->nid,
            'mission_id' => $mission->id,
            'ip' => $request->getClientIp()
        ]);
        //發送全任務完成通知信
        //重新載入玩家資料
        $player = Player::find($player->nid);
        //已全部完成且原本未全部完成
        if ($player->finishAllMissions && !$tempFinishAll) {
            //發送郵件通知
            try {
                $email = $player->nid . '@fcu.edu.tw';
                Mail::queue(
                    'emails.finish',
                    ['nickname' => $player->nid],
                    function ($message) use ($email) {
                        $message->to($email)->subject("【" . Config::get('site.name') . "】恭喜完成全部任務！");
                    }
                );
            } catch (Exception $e) {
                //Log
                LogHelper::info('[SendEmailFailed] 無法寄出過關通知信件給' . $email, [
                    'email' => $email,
                    'ip' => $request->getClientIp()
                ]);
                Log::error($e);
            }
        }
        $json = [
            'success' => true,
            'update' => true
        ];
        return \Response::json($json);
    }

    public function player()
    {
        $player = Player::find(Session::get('nid'));
        if (empty($player)) {
            return null;
        }
        return $player;

    }

    public function getPagesData(Page $page)
    {
        $buttons = [];
        foreach ($page->buttons as $button) {
            $buttons[] = $this->getButtonsData($button);
        }
        $statsPage = $page->statsPage;
        $statsData = [];
        if ($statsPage) {
            $statsData['info'] = $statsPage->info;
            $statsData['time'] = $statsPage->stats['time'];
            $statsData['buttons'] = $statsPage->stats['stats'];
        }
        $data = [
            'info' => $page->info,
            'images' => $page->imageArray,
            'buttons' => $buttons,
            'buttons_count' => count($buttons),
            'typewriter' => (boolean)$page->typewriter,
            'stats_page' => empty($statsData) ? null : $statsData
        ];
        return $data;
    }

    public function getButtonsData(Button $button)
    {
        $data = [
            'id' => $button->id,
            'text' => $button->text,
            'target_page_id' => $button->targetPageIdNum
        ];
        return $data;
    }
}
