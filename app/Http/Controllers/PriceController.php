<?php

namespace App\Http\Controllers;

use App\Player;
use App\Price;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PriceController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //限管理員
        $this->middleware('role:admin');
        //限Ajax
        $this->middleware('ajax', [
            'only' => [
                'ajaxPrice'
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = Price::all();
        return view('price.list')->with('prices', $prices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('price.createOrEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'amount' => 'integer|min:0'
        ]);

        if ($validator->fails()) {
            return redirect()->route('price.create')
                ->withErrors($validator)
                ->withInput();
        }

        $price = Price::create([
            'name' => $request->get('name'),
            'amount' => $request->get('amount')
        ]);

        return redirect()->route('price.index')
            ->with('global', '獎品已新增');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $price = Price::find($id);
        if (!$price) {
            return redirect()->route('price.index')
                ->with('warning', '獎品不存在');
        }
        return view('price.createOrEdit')->with('price', $price);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $price = Price::find($id);
        if (!$price) {
            return redirect()->route('price.index')
                ->with('warning', '獎品不存在');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'amount' => 'integer|min:0'
        ]);

        if ($validator->fails()) {
            return redirect()->route('price.create')
                ->withErrors($validator)
                ->withInput();
        }

        $price->name = $request->get('name');
        $price->amount = $request->get('amount');
        $price->save();

        return redirect()->route('price.index')
            ->with('global', '獎品已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $price = Price::find($id);
        if (!$price) {
            return redirect()->route('price.index')
                ->with('warning', '獎品不存在');
        }
        $price->delete();
        return redirect()->route('price.index')
            ->with('global', '獎品已刪除');
    }

    public function grant()
    {
        return view('price.grant');
    }

    //獎品資訊API（領獎用）
    public function ajaxPrice()
    {
        $prices = Price::all();
        $json = [];
        foreach ($prices as $price) {
            $json[] = [
                'id' => $price->id,
                'name' => $price->name,
                'amount' => $price->amount,
                'left' => $price->left
            ];
        }
        return \Response::json($json);
    }

    //玩家資訊API（領獎用）
    public function ajaxPlayer(Request $request)
    {
        $nid = $request->get('nid');
        $player = Player::find($nid);
        if (!$player) {
            $nid = substr($nid, 0, -1);
            $player = Player::find($nid);
        }
        if (!$player) {
            return \Response::json([]);
        }
        $json = [
            'nid' => $player->nid,
            'finishMissionCount' => $player->finishMissionCount,
            'finishAllMissions' => $player->finishAllMissions,
            'getPriceAt' => $player->get_price_at,
            'getPriceName' => ($player->get_price_at && $player->price) ? $player->price->name : '',
            'canGetPrice' => $player->canGetPrice,
        ];
        return \Response::json($json);
    }


    //領獎
    public function ajaxGrant(Request $request)
    {
        //檢查玩家
        $nid = $request->get('nid');
        $player = Player::find($nid);
        if (!$player) {
            $nid = substr($nid, 0, -1);
            $player = Player::find($nid);
        }
        if (!$player) {
            return \Response::json(['error' => '非有效玩家']);
        }

        if (!$player->canGetPrice) {
            return \Response::json(['error' => '該玩家無法領獎']);
        }
        //檢查獎項
        $priceId = $request->get('price_id');
        $price = Price::find($priceId);
        if (!$price) {
            return \Response::json(['error' => '獎品不存在']);
        }
        if (!$price->left) {
            return \Response::json(['error' => '該獎品已發送完畢']);
        }
        //記錄獎品
        $player->get_price_at = Carbon::now()->toDateTimeString();
        $player->price_id = $price->id;
        $player->save();

        $json = [
            'success' => true,
            'price' => $price->name
        ];

        return \Response::json($json);
    }
}
