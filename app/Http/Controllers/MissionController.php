<?php

namespace App\Http\Controllers;

use App\Button;
use App\Mission;
use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MissionController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //限管理員
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $missions = Mission::with('pages', 'records')->get();
        return view('mission.list')->with('missions', $missions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('mission.index')
                ->withErrors($validator)
                ->withInput()
                ->with('warning', '輸入內容不合規定');
        }

        $mission = Mission::create([
            'title' => $request->get('title')
        ]);

        return redirect()->route('mission.edit', $mission->id)
            ->with('global', '任務順利建立，請繼續設定細節資料');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //使用巢狀預載入減少查詢數
        $mission = Mission::with('pages.buttons.targetPage', 'pages.buttons.page.mission')->find($id);
        if (!$mission) {
            return redirect()->route('mission.index')
                ->with('warning', '任務不存在');
        }
        return view('mission.show')->with('mission', $mission);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mission = Mission::find($id);
        if (!$mission) {
            return redirect()->route('mission.index')
                ->with('warning', '任務不存在');
        }
        return view('mission.edit')->with('mission', $mission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mission = Mission::find($id);
        if (!$mission) {
            return redirect()->route('mission.index')
                ->with('warning', '任務不存在');
        }

        $validator = Validator::make($request->all(), [
            'title' => 'max:255',
            'open_at' => 'date',
            'end_at' => 'date',
            'x' => 'integer|min:0',
            'y' => 'integer|min:0',
        ]);

        if ($validator->fails()) {
            return redirect()->route('mission.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        //預設頁面
        $page = Page::find($request->get('first_page_id'));
        //過關按鈕
        $button = Button::find($request->get('key_button_id'));
        //前置任務（不得選擇本身）
        $requireMission = Mission::where('id', '<>', $mission->id)->find($request->get('require_mission_id'));

        //更新資料
        $mission->title = $request->get('title');
        $mission->open_at = $request->has('open_at') ? $request->get('open_at') : null;
        $mission->end_at = $request->has('end_at') ? $request->get('end_at') : null;
        $mission->first_page_id = ($page) ? $page->id : null;
        $mission->key_button_id = ($button) ? $button->id : null;
        $mission->require_mission_id = ($requireMission) ? $requireMission->id : null;
        $mission->require = ($request->has('require')) ? true : false;
        $mission->x = ($request->get('x') >= 0) ? $request->get('x') : 0;
        $mission->y = ($request->get('y') >= 0) ? $request->get('y') : 0;
        $mission->save();

        return redirect()->route('mission.show', $id)
            ->with('global', '任務已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mission = Mission::find($id);
        if (!$mission) {
            return redirect()->route('mission.index')
                ->with('warning', '任務不存在');
        }
        $mission->delete();

        return redirect()->route('mission.index')
            ->with('global', '任務已刪除');
    }
}
