<?php

namespace App\Http\Controllers;

use App\Helper\LogHelper;
use App\Helper\StringHelper;
use App\Player;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class MailNotifyController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //限管理員
        $this->middleware('role:admin');
    }

    public function index()
    {
        return view('mail-notify.index');
    }

    public function send(Request $request)
    {
        if (!env('ALLOW_MAIL_NOTIFY', false)) {
            return redirect()->route('mail-notify.index')->with('warning', '無法發送通知郵件，請至環境設定檔開啟此功能');
        }
        $players = Player::all();
        $link = route('home') . '?utm_source=Final%20reminder&utm_medium=email&utm_campaign=2015%E8%B3%87%E5%AE%89%E9%80%B1';
        $failList = [];
        $invalidNID = 0;
        foreach ($players as $player) {
            //檢查NID是否符合格式
            if (!StringHelper::isNID($player->nid)) {
                $invalidNID++;
                continue;
            }
            $email = $player->nid . '@fcu.edu.tw';
            try {
                Mail::queue(
                    'emails.notice',
                    ['link' => $link, 'nickname' => $player->nid],
                    function ($message) use ($email) {
                        $message->to($email)->subject("【" . Config::get('site.name') . "】準備好迎向最後的任務了嗎？");
                    }
                );
            } catch (Exception $e) {
                $failList[] = $email;
            }
        }
        if (count($failList)) {
            LogHelper::info('[SendEmailFailed] 無法寄出通知信件以下信箱', $failList);
        }
        return redirect()->route('mail-notify.index')
            ->with('global', '通知郵件已發送，失敗：' . count($failList) . '，無效NID：' . $invalidNID);
    }

}
