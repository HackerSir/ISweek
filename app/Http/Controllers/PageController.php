<?php

namespace App\Http\Controllers;

use App\Mission;
use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //限管理員
        $this->middleware('role:admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mission = Mission::find(Input::get('mission_id'));
        if (!$mission) {
            return redirect()->route('mission.index')
                ->with('warning', '任務不存在');
        }
        return view('page.createOrEdit')->with('mission', $mission);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mission = Mission::find($request->get('mission_id'));
        if (!$mission) {
            return redirect()->route('mission.index')
                ->with('warning', '任務不存在');
        }

        $validator = Validator::make($request->all(), [
            'info' => 'required|max:65535',
            'images' => "regex:/^((https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\/\/=]*))?[\\r\\n]*)+$/"
        ]);

        if ($validator->fails()) {
            return redirect()->route('page.create', ['mission_id' => $mission->id])
                ->withErrors($validator)
                ->withInput();
        }
        $images = preg_split('/(\n|\r|\n\r)/', $request->get('images'), null, PREG_SPLIT_NO_EMPTY);
        $images = implode(PHP_EOL, $images);
        $order = ($mission->pages->count() > 0) ? $mission->pages->max('order') + 1 : 0;
        $statsPage = Page::find($request->get('stats_page_id'));
        $page = Page::create([
            'info' => $request->get('info'),
            'mission_id' => $mission->id,
            'images' => $images,
            'order' => $order,
            'in_stats' => ($request->has('in_stats')) ? true : false,
            'typewriter' => ($request->has('typewriter')) ? true : false,
            'stats_page_id' => ($statsPage) ? $statsPage->id : null
        ]);

        return redirect()->route('mission.show', $mission->id)
            ->with('global', '頁面已新增');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        if (!$page) {
            return redirect()->route('mission.index')
                ->with('warning', '頁面不存在');
        }
        return view('page.createOrEdit')->with('page', $page)->with('mission', $page->mission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        if (!$page) {
            return redirect()->route('mission.index')
                ->with('warning', '頁面不存在');
        }

        $validator = Validator::make($request->all(), [
            'info' => 'required|max:65535',
            'images' => "regex:/^((https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\/\/=]*))?[\\r\\n]*)+$/"
        ]);

        if ($validator->fails()) {
            return redirect()->route('page.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $page->info = $request->get('info');
        $images = preg_split('/(\n|\r|\n\r)/', $request->get('images'), null, PREG_SPLIT_NO_EMPTY);
        $images = implode(PHP_EOL, $images);
        $page->images = $images;
        $page->in_stats = ($request->has('in_stats')) ? true : false;
        $page->typewriter = ($request->has('typewriter')) ? true : false;
        $statsPage = Page::find($request->get('stats_page_id'));
        $page->stats_page_id = ($statsPage) ? $statsPage->id : null;
        $page->save();

        return redirect()->route('mission.show', $page->mission->id)
            ->with('global', '頁面已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);
        if (!$page) {
            return redirect()->route('mission.index')
                ->with('warning', '頁面不存在');
        }
        $mission = $page->mission;
        $page->delete();
        return redirect()->route('mission.show', $mission->id)
            ->with('global', '頁面已刪除');
    }

    public function sort(Request $request)
    {
        //只接受Ajax請求
        if (!$request->ajax()) {
            return 'error';
        }
        $mission = Mission::find($request->get('missionId'));
        if (!$mission) {
            return '任務不存在';
        }
        //若無頁面，直接回傳成功（理論上不會發生）
        if ($mission->pages->count() == 0) {
            return 'success';
        }
        //取得新的id清單
        $idList = $request->get('idList');
        foreach ($idList as $order => $id) {
            $page = Page::find($id);
            $page->order = $order;
            $page->save();
        }
        return 'success';
    }
}
