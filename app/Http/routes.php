<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::get('/activity-rule', [
    'as' => 'activity-rule',
    'uses' => 'HomeController@activityRule'
]);

Route::get('/mission-status', [
    'as' => 'mission-status',
    'uses' => 'HomeController@missionStatus'
]);

Route::get('/privacy', [
    'as' => 'privacy',
    'uses' => 'HomeController@privacy'
]);

Route::get('/terms', [
    'as' => 'terms',
    'uses' => 'HomeController@terms'
]);

Route::get('/FAQ', [
    'as' => 'FAQ',
    'uses' => 'HomeController@FAQ'
]);

Route::get('/staff', [
    'as' => 'staff',
    'uses' => 'HomeController@staff'
]);

//Mission
Route::resource('mission', 'MissionController', [
    'except' => [
        'create'
    ]
]);

//Page
Route::post('page/sort', [
    'as' => 'page.sort',
    'uses' => 'PageController@sort'
]);
Route::resource('page', 'PageController', [
    'except' => [
        'index',
        'show'
    ]
]);

//Button
Route::post('button/sort', [
    'as' => 'button.sort',
    'uses' => 'ButtonController@sort'
]);
Route::resource('button', 'ButtonController', [
    'except' => [
        'index',
        'show'
    ]
]);

//Price
Route::get('price/grant', [
    'as' => 'price.grant',
    'uses' => 'PriceController@grant'
]);
Route::post('price/ajax-price', [
    'as' => 'price.ajax-price',
    'uses' => 'PriceController@ajaxPrice'
]);
Route::post('price/ajax-player', [
    'as' => 'price.ajax-player',
    'uses' => 'PriceController@ajaxPlayer'
]);
Route::post('price/ajax-grant', [
    'as' => 'price.ajax-grant',
    'uses' => 'PriceController@ajaxGrant'
]);
Route::resource('price', 'PriceController', [
    'except' => [
        'show'
    ]
]);

//Player
Route::get('player/export', [
    'as' => 'player.export',
    'uses' => 'PlayerController@export'
]);
Route::resource('player', 'PlayerController', [
    'only' => [
        'index'
    ]
]);

//Mail Notify
Route::get('mail-notify', [
    'as' => 'mail-notify.index',
    'uses' => 'MailNotifyController@index'
]);

Route::post('mail-notify/send', [
    'as' => 'mail-notify.send',
    'uses' => 'MailNotifyController@send'
]);

//Auth System
Route::controller('user', 'UserController', [
    'getIndex' => 'user.list',
    'getLogin' => 'user.login',
    'postLogin' => 'user.login',
    'getRegister' => 'user.register',
    'postRegister' => 'user.register',
    'getConfirm' => 'user.confirm',
    'getResend' => 'user.resend',
    'postResend' => 'user.resend',
    'getForgotPassword' => 'user.forgot-password',
    'postForgotPassword' => 'user.forgot-password',
    'getResetPassword' => 'user.reset-password',
    'postResetPassword' => 'user.reset-password',
    'getChangePassword' => 'user.change-password',
    'postChangePassword' => 'user.change-password',
    'getProfile' => 'user.profile',
    'getEditProfile' => 'user.edit-profile',
    'postEditProfile' => 'user.edit-profile',
    'getEditOtherProfile' => 'user.edit-other-profile',
    'postEditOtherProfile' => 'user.edit-other-profile',
    'getLogout' => 'user.logout'
]);

//API
Route::controller('api', 'ApiController', [
    'postIndex' => 'api.index',
    'postMission' => 'api.mission',
    'postPage' => 'api.page',
    'postButton' => 'api.button',
    'postResult' => 'api.result'
]);

//---------------------------------------------------------------------------------------
//寄送測試信
Route::post('send-test-mail', [
    'as' => 'send-test-mail',
    'uses' => 'SettingController@sendTestMail'
]);

//網站設定
Route::resource('setting', 'SettingController', ['except' => ['create', 'store', 'destroy']]);
//---------------------------------------------------------------------------------------

//Queue狀態
Route::get('queue-status', [
    'as' => 'queue-status',
    'uses' => 'QueueStatusController@index'
]);

//Log Viewer
Route::get('logs', [
    'as' => 'logs',
    'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
    'middleware' => 'role:admin'
]);

//Markdown API
Route::any('markdown', [
    'as' => 'markdown.preview',
    'uses' => 'MarkdownApiController@markdownPreview'
]);

//未定義路由
Route::get('{all}', [
    'as' => 'not-found',
    function () {
        abort(404);
    }
])->where('all', '.*');
