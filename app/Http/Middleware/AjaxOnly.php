<?php

namespace App\Http\Middleware;

use Closure;

class AjaxOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //只接受Ajax請求
        if (!$request->ajax() && !env('APP_DEBUG')) {
            return "error";
        }
        return $next($request);
    }
}
