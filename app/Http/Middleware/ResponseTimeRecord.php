<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Log\Writer;
use Log;
use Monolog\Logger;

//http://stackoverflow.com/questions/31619350/correct-way-to-get-server-response-time-in-laravel
class ResponseTimeRecord
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        try {
            $logger = new Writer(new Logger('ResponseTime'));
            $logger->useDailyFiles(storage_path('response-time/response.log'), 365);

            $responseTime = microtime(true) - LARAVEL_START;
            $logger->info($responseTime . ' "' . $request->getMethod() . ' ' . $request->getPathInfo() . '"');
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
