<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Record extends BaseModel
{
    protected $table = 'records';
    protected $fillable = ['player_nid', 'mission_id', 'ip'];
    protected $primaryKey = ['player_nid', 'mission_id'];

    public function player()
    {
        return $this->belongsTo('App\Player', 'player_nid', 'nid');
    }

    public function mission()
    {
        return $this->belongsTo('App\Mission');
    }

    public function getIsValidAttribute()
    {
        //FIXME 寫買待改善
        $cacheMinute = 0.1;
        $missionEndAt = Cache::remember('missionEndAt_' . $this->mission_id, $cacheMinute, function () {
            $mission = $this->mission;
            return ['endAt' => $mission->end_at];
        });
        $missionEndAt = $missionEndAt['endAt'];
        if (!$missionEndAt) {
            return true;
        }
        if ((new Carbon($this->created_at))->lte(new Carbon($missionEndAt))) {
            return true;
        }
        return false;
    }
}
