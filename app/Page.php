<?php

namespace App;

use App\Helper\StringHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['mission_id', 'info', 'images', 'order', 'in_stats', 'typewriter', 'stats_page_id'];

    public function mission()
    {
        return $this->belongsTo('App\Mission');
    }

    public function buttons()
    {
        return $this->hasMany('App\Button')->orderBy('order');
    }

    public function statsPage()
    {
        return $this->hasOne('App\Page', 'id', 'stats_page_id');
    }

    public function getAbstractAttribute()
    {
        $abstract = '#' . $this->id . ' - ' . StringHelper::cutstr(strip_tags($this->info), 50, '...');
        return $abstract;
    }

    public function getImageLinksAttribute()
    {
        $images = preg_split('/(\n|\r|\n\r)/', $this->images, null, PREG_SPLIT_NO_EMPTY);
        $result = '';
        foreach ($images as $image) {
            $html = link_to($image, $image, ['target' => '_blank']) . '<br />';
            $result .= $html;
        }
        return $result;
    }

    public function getImageArrayAttribute()
    {
        $images = preg_split('/(\n|\r|\n\r)/', $this->images, null, PREG_SPLIT_NO_EMPTY);
        return $images;
    }

    public function getStatsAttribute()
    {
        $cacheMinute = 60;
        $stats = Cache::remember('stats_' . $this->id, $cacheMinute, function () {
            $buttonIds = $this->buttons->lists('id')->toArray();
            $total = ClickRecord::whereIn('button_id', $buttonIds)->count();
            $newStats = [];
            $newStats['time'] = Carbon::now()->toDateTimeString();
            $newStats['stats'] = [];
            foreach ($this->buttons as $button) {
                $count = ClickRecord::where('button_id', '=', $button->id)->count();
                $percent = sprintf("%6.2f", ($total != 0) ? round($count / $total * 100, 2) : 0);
                $newStats['stats'][] = [
                    'text' => $button->text,
                    'count' => $count,
                    'percent' => $percent
                ];
            }
            return $newStats;
        });
        return $stats;
    }


}
