<?php

namespace App;

use App\Helper\StringHelper;
use Illuminate\Database\Eloquent\Model;

class Button extends Model
{
    protected $table = 'buttons';
    protected $fillable = ['page_id', 'text', 'target_page_id', 'order'];

    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    public function clickRecords()
    {
        return $this->hasMany('App\ClickRecord');
    }

    public function targetPage()
    {
        return $this->hasOne('App\Page', 'id', 'target_page_id');
    }

    public function getAbstractAttribute()
    {
        $abstract = $this->text . '（' . $this->page->abstract . '）';
        return $abstract;
    }

    public function getIsKeyButtonAttribute()
    {
        $mission = $this->page->mission;
        if ($mission->key_button_id == $this->id) {
            return true;
        }
        return false;
    }

    public function getTargetPageIdNumAttribute()
    {
        if ($this->isKeyButton) {
            //過關按鈕視為無目標頁面ID
            return null;
        }
        return $this->target_page_id;
    }
}
