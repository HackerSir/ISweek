var $missionButtonTemplate;
var $missionMarker;

var $mapPage;
var $viewMap;
var $mapImage;
var $missionsList;

function mapInit() {
    $missionButtonTemplate = $('<button>').addClass('list-group-item');
    $missionMarker = $('<div>').addClass('map-float btn btn-circle');

    $mapPage = $('#mapPage');
    $viewMap = $('#viewMap');
    $mapImage = $('#mapImage');
    $missionsList = $('#missionsList');

    $(window).resize(function () {
        if ($activePage == $mapPage) {
            handleMarkerXY();
        }
    });

    $('#viewMapOption').on('shown.bs.tab', function (e) {
        handleMarkerXY();
    });
}

function activeMapPage() {
    //地圖
    $viewMap.children('.missionMarker').remove();
    for (var missionsId in activityData.missions) {
        var mission = activityData.missions[missionsId];
        var $marker = $missionMarker.clone();

        $marker.data('missionId', missionsId)
            .click(handleMissionButton);

        $tooltipText = '<h5 style="margin: 5px 0">' + ((mission.require) ? '<span style="color: cyan">[必要任務]</span>&nbsp;' : '') + mission.title + '</h5>';

        $spanClass = '';

        if (!mission.enable) {
            $marker.addClass('btn-danger');
            if (mission.require) {
                $spanClass = 'glyphicon glyphicon-tower';
            }
            else {
                $spanClass = 'glyphicon glyphicon-pawn';
            }

            $marker.addClass('disabled');
            $marker.off('click');

            $tooltipText += '開放時間：' + mission.open_at;

            if (mission.require_mission_id != null) {
                $tooltipText += '<br/>前置任務：' + activityData.missions[mission.require_mission_id].title;
            }
        }
        else {
            if (mission.clear) {
                $marker.addClass('btn-success');
                $spanClass = 'glyphicon glyphicon-ok';
            }
            else {
                if (mission.require) {
                    $marker.addClass('btn-primary');
                    $spanClass = 'glyphicon glyphicon-tower';
                }
                else {
                    $marker.addClass('btn-info');
                    $spanClass = 'glyphicon glyphicon-pawn';
                }
            }
        }
        $marker.append($('<span>', {
            'class': $spanClass
        }));

        $viewMap.append($marker);

        $marker.tooltip({
            placement: 'bottom',
            html: 'true',
            title: $tooltipText
        });
    }

    //任務清單
    $missionsList.empty();
    for (missionsId in activityData.missions) {
        mission = activityData.missions[missionsId];
        var $button = $missionButtonTemplate.clone();
        $button.text(mission.title)
            .data('missionId', missionsId)
            .click(handleMissionButton);

        if (mission.require) {
            $button.append('<i class="fa fa-star pull-right fa-lg fa-fw" style="color: yellowgreen; margin-top: 4px;"></i>');
        }

        if (!mission.enable) {
            $button.append('<span style="display: inline-block;">&nbsp;(開放時間：' + mission.open_at +')</span>');
            if (mission.require_mission_id != null) {
                $button.append('<span style="display: inline-block;">&nbsp;(前置任務：' + activityData.missions[mission.require_mission_id].title + ')</span>');
            }
            $button.append('<i class="fa fa-lock pull-right fa-lg fa-fw" style="margin-top: 4px;"></i>');
            $button.addClass('disabled');
            $button.prop('disabled', true);
        }
        else {
            if (mission.clear) {
                $button.append('<i class="fa fa-check-circle pull-right fa-lg fa-fw" style="color: green; margin-top: 4px;"></i>');
            }
            else {
                $button.append('<span class="badge badge-danger">New</span>');
            }
        }

        $missionsList.append($button);
    }

    $carouselPage.addClass('hidden');
    isMissionStarted = false;

    if (isMobile()) {
        $('#viewListOption').trigger('click');
    }

    //頁面切換
    changePage($mapPage);

    handleMarkerXY();
}

function handleMarkerXY() {
    var $markers = $viewMap.children('.btn-circle');
    var scale = $mapImage.width() / $mapImage[0].naturalWidth;
    for (var i = 0; i < $markers.length; i++) {
        var $marker = $markers.eq(i);
        var mission = activityData.missions[parseInt($marker.data('missionId'))];

        var markerTransform =
            "translate(-50%, -50%)" +
            "scale(" + scale + ")" +
            "translate(" + mission.x + "px," + mission.y + "px)";

        $marker.css({
            '-ms-transform': markerTransform,
            '-webkit-transform': markerTransform,
            '-moz-transform': markerTransform,
            '-o-transform': markerTransform,
            'transform': markerTransform
        });
    }
}
