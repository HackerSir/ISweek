var $loadingIcon;

var $activePage;

var $startPage;
var $username;
var $startButton;

var nid;
var activityData;

$(function () {
    $loadingIcon = $('#loadingIcon');

    $startButton = $('#startButton');
    $startPage = $('#startPage');
    $username = $('#username');

    dialogInit();
    mapInit();

    $activePage = $startPage;

    $startButton.click(sendNID);

    $username.keydown(function (e) {
        // the enter key code
        if (e.which == 13) {
            if (!$startButton.prop('disabled')) {
                $startButton.click();
            }
            return false;
        }
        return true;
    });

    $(document).keypress(function (e) {
        if ($activePage == $dialogPage) {
            if (e.which == 32) {
                handleSpaceKeyInDialogPage();
                return false;
            }
        }
        return true;
    });

    $('#skipLink').click(handleDialogPageButton);

    $('input[name="viewOptions"]').change(function () {
        $(this).tab('show');
    });

    //使用者可能會在頁面未載入完成輸入，初始化完成後檢查
    $startButton.removeAttr("disabled");
});

function changePage($nextPage) {
    //切換頁面時，捲動到最上方
    $("html, body").animate({scrollTop: 0}, "fast");
    //頁面切換
    $activePage.addClass('hidden');
    $nextPage.removeClass('hidden');
    $activePage.removeClass('zoomIn');
    $activePage = $nextPage;
    $activePage.addClass('zoomIn');
}

function isMobile() {
    return window.innerWidth < 768;
}

function sendNID() {
    //Check NID
    if ($username.val() == "") {
        alert('請輸入"學號"或"教職員證號"');
        return;
    }

    $loadingIcon.removeClass('hidden');

    nid = $username.val().replace(/\s/g, '');
    $.ajax({
        url: api_index,
        data: {'nid': nid},
        headers: {
            'X-CSRF-Token': Xtoken,
            "Accept": "application/json"
        },
        type: "POST",
        dataType: "json",

        success: function (data) {
            if (data.hasOwnProperty('error')) {
                show_message('error', data.error);
                return;
            }
            $('#usernameTitle').text('嗨！' + nid);
            activityData = data;
            var page = {
                info: activityData.story,
                buttons: [{id: '-1', text: '進入地圖', target_page_id: -1}],
                buttons_count: '1',
                images: [],
                typewriter: true,
                stats_page: null
            };
            activeDialogPage(page, true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            show_message('error', xhr.status + ': ' + thrownError);
        },
        complete: function (jqXHR, textStatus) {
            $loadingIcon.addClass('hidden');
        }
    });
}

function sendFinish() {
    $loadingIcon.removeClass('hidden');
    $.ajax({
        url: api_result + "/" + missionId,
        headers: {
            'X-CSRF-Token': Xtoken,
            "Accept": "application/json"
        },
        data: {
            'nid': nid.replace(/\s/g, ''),
            "record": record,
            "m_token": missionData.m_token
        },
        type: "POST",
        dataType: "json",

        success: function (data) {
            if (data.hasOwnProperty('error')) {
                show_message('error', data.error);
                return;
            }
            if (data.success) {
                show_message('success', '任務完成');
            }
            if (data.update) {
                activityData.missions[missionId].clear = true;

                var now = dateToString(new Date());
                for (var id in activityData.missions) {
                    if (now > activityData.missions[id].open_at && activityData.missions[id].require_mission_id == missionId) {
                        activityData.missions[id].require_mission_id = null;
                        activityData.missions[id].enable = true;
                    }
                }

                missionId = -1;
            }
            activeMapPage();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            show_message('error', xhr.status + ': ' + thrownError);
        },
        complete: function (jqXHR, textStatus) {
            $loadingIcon.addClass('hidden');
        }
    });
}

function dateToString(date) {
    var months = date.getMonth() + 1;
    if (months < 10) months = '0' + months;

    var days = date.getDate();
    if (days < 10) days = '0' + days;

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var minutes = date.getMinutes();
    if (minutes < 10) minutes = '0' + minutes;

    var seconds = date.getSeconds();
    if (seconds < 10) seconds = '0' + seconds;

    return date.getFullYear() + '-' + months + '-' + days + " " +
        hours + ':' + minutes + ':' + seconds;
}
