var $TextButtonTemplate;
var $progressTemplate;
var $progressBarTemplate;

var $dialogPage;
var $textArea;
var $pushSpaceKeyTip;
var $dialogPageButtonGroup;
var $showAllTextLink;

var $carouselPage;
var $missionCarousel;
var $div_ol;
var $div_image;

var $statsPage;
var $statsInfo;
var $statsButton;
var $updateTime;

function dialogInit() {
    $TextButtonTemplate = $('<button>').addClass('btn btn-lg');
    $progressTemplate = $('<div>').addClass('progress');
    $progressBarTemplate = $('<div>').addClass('progress-bar')
        .attr('aria-valuemin', '0')
        .attr('aria-valuemax', '100')
        .attr('role', 'progressbar');

    $dialogPage = $('#dialogPage');
    $textArea = $('#textArea');
    $pushSpaceKeyTip = $('#pushSpaceKeyTip');
    $dialogPageButtonGroup = $('#dialogPageButtonGroup');
    $showAllTextLink = $('#showAllTextLink');

    $carouselPage = $('#carouselPage');
    $missionCarousel = $('#mission-carousel');
    $div_ol = $missionCarousel.find('.carousel-indicators');
    $div_image = $missionCarousel.find('.carousel-inner');

    $statsPage = $('#statsPage');
    $statsInfo = $('#statsInfo');
    $statsButton = $('#statsButton');
    $updateTime = $('#updateTime');

    $showAllTextLink.click(function () {
        addAllText();
    });
}

var pageData;

function activeDialogPage(page, showSkipLink) {
    showSkipLink = showSkipLink || false;
    var $skipLink = $('#skipLink');
    if (showSkipLink) {
        $skipLink.show();
    }
    else {
        $skipLink.hide();
    }
    pageData = page;

    $pushSpaceKeyTip.addClass('hidden');

    $dialogPageButtonGroup.empty();

    //清空文字
    $textArea.text('');

    //初始化圖片輪播
    $carouselPage.addClass('hidden');
    if (page.images.length > 0) {
        //清空圖片
        $div_ol.empty();
        $div_image.empty();
        //新增圖片
        for (var index in page.images) {
            $div_ol.append($('<li>', {
                "data-target": "#mission-carousel",
                "data-slide-to": index
            }));

            var $img = $('<img>', {
                "src": page.images[index],
                "class": 'img-responsive'
            });

            $div_image.append($('<div>', {
                "class": "item"
            }).append($img));
        }
        $div_ol.children().first().addClass('active');
        $div_image.children().first().addClass('active');
        $carouselPage.removeClass('hidden');
    }

    $statsPage.addClass('hidden');
    if (page.stats_page != null) {
        $statsInfo.html(page.stats_page.info);
        $updateTime.text(page.stats_page.time);
        $statsButton.empty();
        for (var index in page.stats_page.buttons) {
            var buttonStat = page.stats_page.buttons[index];
            $statsButton.append($('<div>', {
                'class': 'col-md-2'
            }).text(buttonStat.text));
            $statsButton.append($('<div>', {
                'class': 'col-md-10'
            }).append($progressTemplate.clone().append($progressBarTemplate.clone()
                .attr("aria-valuenow", buttonStat.percent)
                .css('width', buttonStat.percent + '%')
                .text(buttonStat.count + ' (' + buttonStat.percent + '%)')
            )));
        }
    }

    //頁面切換
    changePage($dialogPage);

    buttonsData = page.buttons;
    buttonsCount = page.buttons_count;

    if (page.typewriter) {
        $showAllTextLink.removeClass('hidden');
        clearAllSpan();
        typedQueue = typedQueue.concat(page.info.split('^pause'));
        typeRunning = false;
        playText();
    }
    else {
        addAllText();
    }

    if (page.images.length > 0) {
        $('html,body').animate({scrollTop: $carouselPage.offset().top - 65});
    }
}

function addAllText() {
    clearAllSpan();
    $textArea.append($('<span>').html(pageData.info.replace(/\^pause/g, '').replace(/\^[0-9]+/g, '')).css({
        'white-space': 'pre-wrap'
    }));
    addButtons();
}

function clearAllSpan() {
    //清空 typedQueue
    typedQueue = [];

    var spans = $textArea.children('span');
    for (var i = 0; i < spans.length; i++) {
        var span = spans.eq(i);
        span.attr('close', true);
    }
    $textArea.empty();
}

//防止在播放時又播放下一組
var typeRunning;
var typedQueue = [];

function playText() {
    if (typeRunning) return;
    if (typedQueue.length > 0) {
        var $span = $('<span>');
        $textArea.append($span.css('white-space', 'pre-wrap'));
        $textArea.children('.typed-cursor').remove();

        typeRunning = true;
        $pushSpaceKeyTip.addClass('hidden');
        $span.typed({
            strings: [typedQueue.shift()],
            typeSpeed: 10,
            callback: function () {
                typeRunning = false;
                if ($span.attr('close')) return;
                if (isMobile() && typedQueue.length > 0) {
                    //手機自動播放
                    playText();
                }
                else if (typedQueue.length == 0) {
                    //播放完成新增按紐
                    addButtons();
                }
                else {
                    $pushSpaceKeyTip.removeClass('hidden');
                }
            }
        });
    }
}

function handleSpaceKeyInDialogPage() {
    //先嘗試撥放下一組文字
    playText();

    if (buttonsCount == 1 && $carouselPage.hasClass('hidden')) {
        $dialogPageButtonGroup.children()[0].click();
    }
}

function addButtons() {
    //如果有按鈕了，就不要新增
    if ($dialogPageButtonGroup.children().length > 0) {
        return;
    }
    //加入按鈕
    for (var index in buttonsData) {
        var button = buttonsData[index];
        var target_page_id = button.target_page_id;
        if (target_page_id == null) target_page_id = -2;

        var $button = $TextButtonTemplate.clone()
            .text(button.text)
            .data('target_page_id', target_page_id)
            .data('buttonId', button.id)
            .click(handleDialogPageButton);

        if (button.id != -1 && missionData.key_button_id == button.id) {
            $button.addClass('btn-success');
        }
        else {
            $button.addClass('btn-primary');
        }

        $dialogPageButtonGroup.append($button);
    }
    if (buttonsCount > 1) {
        $pushSpaceKeyTip.addClass('hidden');
    }
    else if (buttonsCount == 1 && $carouselPage.hasClass('hidden')) {
        $pushSpaceKeyTip.removeClass('hidden');
    }
    $showAllTextLink.addClass('hidden');
    if (pageData.stats_page != null) {
        $statsPage.removeClass('hidden');
    }
}

function handleDialogPageButton() {
    var nextPageId = $(this).data('target_page_id');
    var buttonId = $(this).data('buttonId');
    if (nextPageId == -1) {
        //跳過
        clearAllSpan();
        activeMapPage();
    }
    else {
        record.push(buttonId);
        //如果按鈕是過關按鈕
        if (missionData.key_button_id == buttonId) {
            $carouselPage.addClass('hidden');
            $statsPage.addClass('hidden');
            sendFinish();
        }
        else {
            if (nextPageId in missionData.pages) {
                var nextPage = missionData.pages[nextPageId];
                activeDialogPage(nextPage);
            }
            else {
                show_message('info', '這個按鈕沒有下一頁');
            }
        }
    }
}
