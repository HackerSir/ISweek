var record;
var missionId;
var missionData;

function startMission(data) {
    missionData = data;
    if (missionData.pages_count == 0) {
        show_message('error', '(Code: 0x01)關卡設定有問題，請聯絡管理員。');
        return;
    }

    if (missionData.first_page_id == null) {
        show_message('error', '(Code: 0x02)關卡設定有問題，請聯絡管理員。');
        return;
    }

    if (missionData.key_button_id == null) {
        show_message('error', '(Code: 0x03)關卡設定有問題，請聯絡管理員。');
        return;
    }

    isMissionStarted = true;
    record = [];
    var startPage = missionData.pages[missionData.first_page_id];
    activeDialogPage(startPage);
}

function handleMissionButton() {
    missionId = $(this).data('missionId');
    $loadingIcon.removeClass('hidden');
    $.ajax({
        url: api_mission + "/" + missionId,
        headers: {
            'X-CSRF-Token': Xtoken,
            "Accept": "application/json"
        },
        type: "POST",
        dataType: "json",

        success: function (data) {
            if (data.hasOwnProperty('error')) {
                show_message('error', data.error);
                return;
            }
            startMission(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            show_message('error', xhr.status + ': ' + thrownError);
        },
        complete: function (jqXHR, textStatus) {
            $loadingIcon.addClass('hidden');
        }
    });
}
