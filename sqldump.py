#!/usr/bin/python3
import sys
import os
import time

path = os.path.dirname(os.path.realpath(__file__)) + "/"
if len(sys.argv) >= 2:
  dotenv_path = sys.argv[1]
elif os.path.exists(path + ".env"):
  dotenv_path = path + ".env"
else:
  dotenv_path = None

# Load env
env = {}
if dotenv_path is not None:
  with open(dotenv_path) as f:
    for data in f:
      if data[0] != "#":
        data = data.strip().split('=')
        if data[0] != '':
          data[1] = data[1].split('#')[0].strip()
          env[data[0]] = str(data[1])

# Check env
if env is None:
  print("Pass, .env does not exist")
  pass
elif env.get("APP_ENV") != "production" or env.get("APP_DEBUG") == "true":
  print("Pass, a test env?")
  pass
else:
  # DB Configure
  host = env.get("DB_HOST")
  db = env.get("DB_DATABASE")
  username = env.get("DB_USERNAME")
  password = env.get("DB_PASSWORD")

  # Target
  path = path + "/storage/sqldump/"
  if not os.path.exists(path):
    os.makedirs(path)
    with open(path + ".gitignore", "w") as ignorefile:
      ignorefile.write("*\n!.gitignore")
      ignorefile.close()

  target = path + db + "-" + time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime()) + ".sql"

  # Dump and compress
  os.system("mysqldump -h " + host + " -u " + username + " --password=" + password + " " + db + " > " + target)
  os.system("xz -z " + target)


