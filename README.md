# IS Week
A website for Feng Chia Universiry Information Security Week.  
We hope everyone can learn Information Security from it easily.  
No stress, just pure fun.

# Requirement
- Apache 2.4.17+ or Nginx 
- PHP 5.5.9+ or PHP-fpm instead
- MySQL (MariaDB)
- Composer
- Nodejs w/ gulp


# Deployment
0. Prepare your environment. (Make sure that you have installed all apps above)
```
# For gulp installed (you will need it to build bootflat)
npm install --global gulp
```
1. Clone this Project into your server.  
 `` git clone https://gitlab.com/HackerSir/ISweek.git ``
2. Use Composer to install plugins.
 ```
 cd ISweek
 composer install
 ```
3. Add this website into your WebServer (ex: VirtualHost)  
  
4. Install package (in your project directory)  
`` npm install ``

# Staffs
Thanks for the following people for assistance:     

## Environment  
- Vongola <vongola12324>  

## Develop
- Danny <danny50610>  
- KID <jyhsu2000>  

## Art
  
## Administrative