<?php

use Illuminate\Support\Facades\Auth;

return [
    /**
     * 巡覽列
     *
     * 基本格式：'連結名稱' => '連結路由'
     *
     * 多層：二級選單以下拉式選單呈現，更多層級以巢狀顯示，太多層可能會超過螢幕顯示範圍
     * 外部連結：在連結路由部分，直接填上完整網址（開頭需包含協定類型）
     * 新分頁開啟：外部連結或路由開頭為「!」者會由新分頁開啟
     */

    //基本巡覽列
    'navbar' => [
        '活動辦法' => 'activity-rule',
        '前線戰況' => 'mission-status',
    ],
    //會員
    'member' => [
        '%user%' => [
            '個人資料' => 'user/profile',
            '修改密碼' => 'user/change-password',
            '登出' => 'user/logout'
        ]
    ],
    //管理員
    'admin' => [
        '駭客出任務' => [
            '任務管理' => 'mission',
            '獎品管理' => 'price',
            '玩家管理' => 'player'
        ],
        '管理員' => [
            '成員清單' => 'user',
            '網站設定' => 'setting',
            '郵件通知' => 'mail-notify',
            'Queue狀態' => 'queue-status',
            '記錄檢視器' => '!logs'
        ]
    ],
    //遊客
    'guest' => []
];
