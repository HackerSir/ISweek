<!DOCTYPE html>
@include('egg.source-view')
<html lang="zh-Hant-TW">
    <head>
        {{-- MetaTag --}}
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="{{ Config::get('site.desc') }}">

        {{-- MetaTag(OpenGraph) --}}
        <meta property="og:title"
              content="@if(trim($__env->yieldContent('title'))) @yield('title') - @endif{{ Config::get('site.name') }}">
        <meta property="og:url" content="{{ URL::current() }}">
        <meta property="og:image" content="">
        <meta property="og:description" content="{{ Config::get('site.desc') }}">
        <meta property="og:locale" content="zh_TW">
        <meta property="og:site_name" content="{{ Config::get('site.name') }}">

        {{-- Title --}}
        <title>@if (trim($__env->yieldContent('title'))) @yield('title') - @endif{{ Config::get('site.name') }}</title>

        {{-- Sheetstyle --}}
        {!! HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') !!}
        {!! HTML::style(elixir('build-css/bootflat.css')) !!}
        {!! HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') !!}
        {!! Minify::stylesheet([
            '/css/app.css',              // 全域自訂 CSS
            '/css/sticky-footer.css',    // 頁尾資訊
            '/css/animate.css',          // 淡入淡出特效
            '/css/tipped.css',           // 好看的提示框
        ])->withFullUrl() !!}
        {!! Minify::stylesheet([
            '/build-css/pnotify.css',     //pnotify
            '/build-css/pnotify.buttons.css',
            '/build-css/pnotify.mobile.css',
        ])->withFullUrl() !!}
        @yield('css')

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            {!! HTML::script('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') !!}
            {!! HTML::script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') !!}
        <![endif]-->
        @yield('head-javascript')
    </head>
    <body>
        @if(App::environment('production'))
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-61683465-6', 'auto');
            ga('send', 'pageview');
        </script>
        @endif

        {{-- Navbar --}}
        @include('common.navbar')

        {{-- Content --}}
        @yield('content')

        {{-- Footer --}}
        <footer class="footer">
            <div class="container" style="padding-left: 0; padding-right: 0">
                <p class="text-muted">
                    Powered by <a href="https://hackersir.org" title="黑客社網站" target="_blank">逢甲大學黑客社</a><a href="https://www.facebook.com/HackerSir.tw" title="FB粉絲專頁" target="_blank"><span class="fa fa-lg fa-facebook-official" style="margin-left: 5px"></span></a>
                </p>
                <ol class="breadcrumb" style="padding-top: 0; margin-bottom: 0; background-color: #f5f5f5;">
                    <li>{!! HTML::linkRoute('privacy', '隱私權') !!}</li>
                    <li>{!! HTML::linkRoute('terms', '服務條款') !!}</li>
                    <li>{!! HTML::linkRoute('FAQ', '常見問題') !!}</li>
                    <li>{!! HTML::linkRoute('staff', '工作人員') !!}</li>
                    <li><a href="mailto:{{ urlencode('"駭客出任務"') }}<ics@mail.fcu.edu.tw>" target="_blank"><span class="glyphicon glyphicon-envelope" aria-hidden="true" style="margin-right: 5px;"></span>聯絡我們</a></li>
                    @if(Auth::guest())
                        <li class="pull-right" id="login">
                            {!! HTML::linkRoute('user.login', '登入', null, [
                                'style' => 'color: #f5f5f5;',
                            ]) !!}
                        </li>
                    @endif
                </ol>
            </div>
        </footer>

        {{-- Javascript --}}
        {!! HTML::script('//code.jquery.com/jquery-2.1.4.min.js') !!}
        {!! HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js') !!}

        {{-- PNotify --}}
        {!! Minify::javascript([
            '/build-js/pnotify.js',
            '/build-js/pnotify.buttons.js',
            '/build-js/pnotify.mobile.js',
            '/build-js/pnotify.animate.js'
        ])->withFullUrl() !!}

        {!! Minify::javascript([
            '/js/tipped.js',  //好看的提示框
            '/js/module-pnotify.js'
        ])->withFullUrl() !!}

        <script type="text/javascript">
        @if(Session::has('global'))
            show_message('success', '{!! Session::get('global') !!}');
        @endif
        @if(Session::has('warning'))
            show_message('error', '{!! Session::get('warning') !!}');
        @endif
        @if(Session::has('info'))
            show_message('info', '{!! Session::get('info') !!}');
        @endif
        $(document).ready(function() {
            Tipped.create('*',{
                fadeIn: 0,
                fadeOut: 0,
                position: 'right',
                target: 'mouse',
                showDelay: 0,
                hideDelay: 0,
                offset: { x: 0, y: 15 },
                stem: false
            });
        });
        </script>
        @include('egg.console')

        @yield('js')

        <script src="//fast.wistia.net/labs/fresh-url/v1.js" async></script>
    </body>
</html>
