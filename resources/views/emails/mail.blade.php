<!DOCTYPE html>
<html lang="zh-TW">
    <head>
        <meta charset="utf-8">
    </head>
    <body style="margin: auto;width:50%">
        <br>
        {!! HTML::image('img/HackerInMission.png', 'Logo', ['style' => 'vertical-align: middle;display: block;margin: 0 auto;width: 30%']) !!}
        <hr style="text-align: center;width: 50%">
        @yield('content')
        <p>
            此致,<br/>
            逢甲大學資安週執行委員會 <br/>
            {!! Carbon::today()->format('Y/m/d') !!}
        </p>
        <p>
            ----- <br/>
            這是一封系統發出的信件，請勿回覆。<br/>
            若有任何問題，歡迎聯絡我們，我們會竭盡心力協助您處理。
        </p>
    </body>
</html>
