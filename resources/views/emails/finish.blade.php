@extends('emails.mail')

@section('content')
    <h2 style="text-align: center">嘿！ {{ $nickname }}！</h2>
    <h2 style="text-align: center">恭喜您，您已經完成資安週的所有指定任務！</h2>
    <p>
        <br />
        請您<span style="color: red"><strong>攜帶學生證</strong></span>，並在「<span style="color: red"><strong>2015年12月23日 10點00分 ～ 17點00分</strong></span>」這段時間內到資安週擺攤處抽獎！（數量有限，送完為止）<br />
        屆時恭候您的蒞臨，我們的擺攤位置在「<span style="color: red"><strong>忠勤樓前（與工學院的轉角處）</strong></span>」，在此附上位置圖供您參考。<br />
        <br />
        {{-- 地圖 --}}
        {!! HTML::image('img/ISCMap.png', 'Logo', ['style' => 'vertical-align: middle;display: block;margin: 0 auto;width: 50%']) !!}
    </p>
    <br />
@endsection
