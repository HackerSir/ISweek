@extends('emails.mail')

@section('content')
    <h2 style="text-align: center">嘿 {{ $nickname }}，準備好迎向最後的任務了嗎？</h2>
    <p>
        最後一個任務將在12月22日的午夜現身！你，準備好了嗎？<br/>
        點擊下面這個連結來進行任務吧！<br/>
        <a href="{{ $link }}">{{ $link }}</a><br/>
        <br/>
    </p>
    <p>
        如果上面的網址不是連結，請您將該網址複製到瀏覽器(IE、Firefox、Chrome等)的網址列。<br/>
        只要是逢甲大學學生及教職員工，並且完成五個主線任務，就可以獲得抽獎的機會，獎品有限，抽完為止，詳細請看{!! HTML::linkRoute('activity-rule', '活動辦法') !!}！
    </p>
    <br/>
@endsection
