@extends('app')

@section('title')編輯任務@endsection

@section('css')
    {!! HTML::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        編輯任務
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            {!! Form::open(['route' => ['mission.update', $mission->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                            <div class="form-group has-feedback{{ ($errors->has('title'))?' has-error':'' }}">
                                {!! Form::label('title', '任務標題', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('title', $mission->title, ['required', 'placeholder' => '請輸入任務標題', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('open_at'))?' has-error':'' }}">
                                {!! Form::label('open_at', '開始時間', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    <div class='input-group date' id='datetimepicker1'>
                                        {!! Form::text('open_at', $mission->open_at, ['id' => 'open_at', 'placeholder' => 'YYYY/MM/DD HH:mm:ss', 'class' => 'form-control']) !!}
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    @if($errors->has('open_at'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('open_at') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('end_at'))?' has-error':'' }}">
                                {!! Form::label('end_at', '計分截止時間', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    <div class='input-group date' id='datetimepicker2'>
                                        {!! Form::text('end_at', $mission->end_at, ['id' => 'end_at', 'placeholder' => 'YYYY/MM/DD HH:mm:ss', 'class' => 'form-control']) !!}
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    @if($errors->has('end_at'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('end_at') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('first_page_id'))?' has-error':'' }}">
                                {!! Form::label('first_page_id', '初始頁面', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::select('first_page_id', $mission->pageSelections, $mission->first_page_id, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('key_button_id'))?' has-error':'' }}">
                                {!! Form::label('key_button_id', '過關按鈕', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::select('key_button_id', $mission->buttonSelections, $mission->key_button_id, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('require_mission_id'))?' has-error':'' }}">
                                {!! Form::label('require_mission_id', '前置任務', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::select('require_mission_id', $mission->missionSelections, $mission->require_mission_id, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('require'))?' has-error':'' }}">
                                {!! Form::label('require', '必要任務', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    <label>
                                        {!! Form::checkbox('require', true, $mission->require) !!}
                                        必要任務（未通過將無法領取獎品）
                                    </label>
                                </div>
                            </div>
                            <div class="form-group has-feedback {{ ($errors->has('x'))?' has-error':'' }}">
                                {!! Form::label('x', 'X座標', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::number('x', $mission->x, ['required', 'class' => 'form-control', 'min' => 0]) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback {{ ($errors->has('y'))?' has-error':'' }}">
                                {!! Form::label('y', 'Y座標', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::number('y', $mission->y, ['required', 'class' => 'form-control', 'min' => 0]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-2">
                                    {!! Form::submit('更新任務', ['class' => 'btn btn-primary']) !!}
                                    {!! HTML::linkRoute('mission.show', '返回', $mission->id, ['class' => 'btn btn-default']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js') !!}
    {!! Minify::javascript('/js/moment_zh-tw.js')->withFullUrl() !!}
    {!! HTML::script('js/bootstrap-datetimepicker.min.js') !!}
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY/MM/DD HH:mm:ss'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'YYYY/MM/DD HH:mm:ss'
            });
        });
        $(document).ready(function(){
            $('#require_mission_id > option[value="{{ $mission->id }}"]').attr('disabled', 'disabled');
        });
    </script>
@endsection
