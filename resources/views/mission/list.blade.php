@extends('app')

@section('title')任務管理@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        任務管理
                    </div>
                    <div class="panel-body">
                        <div class="pull-right">
                            {!! Form::open(['route' => 'mission.store', 'id' => 'popup_form', 'class' => 'form-inline']) !!}
                            <div class="form-group has-feedback{{ ($errors->has('title'))?' has-error':'' }}">
                                {!! Form::text('title', null, ['placeholder' => '請輸入任務名稱', 'required', 'class' => 'form-control']) !!}
                                @if($errors->has('title'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>@endif
                            </div>
                            {!! Form::submit('新增任務', ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>任務名稱</th>
                                    <th>開始時間</th>
                                    <th>計分截止時間</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($missions as $mission)
                                    <tr @if(!count($mission->pages) || !$mission->first_page_id || !$mission->key_button_id) class="danger" @endif>
                                        <td>
                                            {!! link_to_route('mission.show', $mission->title, $mission->id) !!}
                                            <span class="pull-right">
                                                @if($mission->pages->count())
                                                    <span title="共 {{ $mission->pages->count() }} 頁">
                                                        <span class="fa fa-file-text-o"></span>
                                                        {{ $mission->pages->count() }}
                                                    </span>
                                                @endif
                                                @if($mission->records->count())
                                                    <span title="共 {{ $mission->records->count() }} 人完成任務">
                                                        <span class="fa fa-users"></span>
                                                        {{ $mission->records->count() }}
                                                    </span>
                                                @endif
                                                @if($mission->require)
                                                    <span class="fa fa-check text-success" title="必要任務"></span>
                                                @endif
                                                @if(!count($mission->pages))
                                                    <span class="fa fa-exclamation-triangle" title="此任務無任何頁面"></span>
                                                @endif
                                                @if(!$mission->first_page_id)
                                                    <span class="fa fa-exclamation-triangle" title="此任務無預設頁面"></span>
                                                @endif
                                                @if(!$mission->key_button_id)
                                                    <span class="fa fa-exclamation-triangle" title="此任務無過關按鈕"></span>
                                                @endif
                                            </span>
                                        </td>
                                        <td>{{ $mission->open_at }}</td>
                                        <td>{{ $mission->end_at }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            暫無任務
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
