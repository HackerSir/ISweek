@extends('app')

@section('title'){{ $mission->title }} - 任務資訊@endsection

@section('css')
    <style type="text/css">
        .panel-collapse {
            border: 1px solid #eee;
        }
        .button-list .row{
            border: 1px solid #eee;
        }
        .sort-icon {
            cursor: move;
        }
        .placeholder {
            min-height: 60px;
            border: 3px dashed grey;
            border-radius: 10px;
            text-align: center;
            font-size: 2em;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $mission->title }} - 任務資訊
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <th>任務標題</th>
                                <td>{{ $mission->title }}</td>
                            </tr>
                            <tr>
                                <th>開始時間</th>
                                <td>
                                    <span title="{{ (new Carbon($mission->open_at))->diffForHumans() }}">{{ $mission->open_at }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>計分截止時間</th>
                                <td>
                                    <span title="{{ (new Carbon($mission->end_at))->diffForHumans() }}">{{ $mission->end_at }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>初始頁面</th>
                                @if($mission->firstPage)
                                    <td>
                                        {{  $mission->firstPage->abstract }}
                                    </td>
                                @else
                                    <td class="danger">
                                        <span style="color: grey">（尚未設定）</span>
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <th>過關按鈕</th>
                                @if($mission->keyButton)
                                    <td>
                                        {{ $mission->keyButton->abstract }}
                                    </td>
                                @else
                                    <td class="danger">
                                        <span style="color: grey">（尚未設定）</span>
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <th>前置任務</th>
                                @if($mission->requireMission)
                                    <td>
                                        {{ $mission->requireMission->title }}
                                    </td>
                                @else
                                    <td>
                                        <span style="color: grey">（無）</span>
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <th>必要任務</th>
                                <td>
                                    @if($mission->require)
                                        <span class="fa fa-check text-success"></span> 未通過將無法領取獎品
                                    @else
                                        <span class="fa fa-times text-danger"></span> 不影響領獎
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>座標</th>
                                <td>
                                    ({{ $mission->x }}, {{ $mission->y }})
                                </td>
                            </tr>
                        </table>
                        <div class="text-center">
                            {!! link_to_route('mission.edit', '編輯', $mission->id, ['class' => 'btn btn-default']) !!}
                            {!! Form::open(['route' => ['mission.destroy', $mission->id], 'style' => 'display: inline', 'method' => 'DELETE',
                            'onSubmit' => "return confirm('確定要刪除此任務嗎？');"]) !!}
                            {!! Form::submit('刪除', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                            {!! link_to_route('mission.index', '返回清單', [], ['class' => 'btn btn-default']) !!}
                        </div>
                    </div>
                </div>
                <div class="panel @if(count($mission->pages)) panel-default @else panel-danger @endif">
                    <div class="panel-heading">
                        頁面清單
                        @if(!count($mission->pages))
                            <span class="fa fa-exclamation-triangle" title="此任務無任何頁面"></span>
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <span class="mission-status-text"></span>
                            </div>
                            <div class="pull-right">
                                {!! link_to_route('page.create', '新增頁面', ['mission_id' => $mission->id], ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            @forelse($mission->pages as $page)
                                <div class="panel @if(count($page->buttons)) panel-default @else panel-danger @endif" data-id="{{ $page->id }}">
                                    <div class="panel-heading" role="tab" id="heading_{{ $page->id }}">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $page->id }}" aria-expanded="true" aria-controls="collapse_{{ $page->id }}">
                                                <span class="sort-icon" style="margin-left: 10px;"><span class="glyphicon glyphicon-sort"></span></span>
                                                {{ $page->abstract }}
                                                @if(count($page->imageArray))
                                                    <span class="fa fa-picture-o" title="此頁包含 {{ count($page->imageArray) }} 張圖片"></span>
                                                @endif
                                                @if($page->in_stats)
                                                    <span class="fa fa-bar-chart" title="此頁列入統計"></span>
                                                @endif
                                                @if($page->typewriter)
                                                    <span class="fa fa-keyboard-o" title="打字機效果"></span>
                                                @endif
                                                @if(!count($page->buttons))
                                                    <span class="fa fa-exclamation-triangle" title="此頁面無按鈕"></span>
                                                @endif
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_{{ $page->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{ $page->id }}">
                                        <div class="panel-body">
                                            <div>
                                                {!! \App\Helper\StringHelper::nl2br($page->info) !!}
                                            </div>
                                            @if(count($page->imageArray))
                                                <div>
                                                    <span style="font-size: 1.3em; font-weight:bold;">圖片清單</span><br />
                                                    {!! $page->imageLinks !!}
                                                </div>
                                            @endif
                                            @if($page->statsPage)
                                                <div>
                                                    <span style="font-size: 1.3em; font-weight:bold;">顯示以下頁面之按鈕統計</span><br />
                                                    {{ $page->statsPage->abstract }}
                                                </div>
                                            @endif
                                            <div>
                                                {!! link_to_route('page.edit', '編輯', $page->id, ['class' => 'btn btn-default']) !!}
                                                {!! Form::open(['route' => ['page.destroy', $page->id], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此頁面嗎？');"]) !!}
                                                {!! Form::submit('刪除', ['class' => 'btn btn-danger']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                            {{-- 按鈕清單 --}}
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <span style="font-size: 1.3em; font-weight:bold;">按鈕清單</span>
                                                    <span class="status-text" data-page-id="{{ $page->id }}"></span>
                                                </div>
                                                <div class="col-md-4">
                                                    {!! link_to_route('button.create', '新增按鈕', ['page_id' => $page->id], ['class' => 'btn btn-default pull-right']) !!}
                                                </div>
                                            </div>
                                            <div class="container-fluid button-list">
                                                <div class="row">
                                                    <div class="col-md-1 text-center"><strong>排序</strong></div>
                                                    <div class="col-md-3"><strong>按鈕文字</strong></div>
                                                    <div class="col-md-5"><strong>目標頁面</strong></div>
                                                    <div class="col-md-3"><strong>操作</strong></div>
                                                </div>
                                                <div class="button-items" data-page-id="{{ $page->id }}">
                                                    @forelse($page->buttons as $button)
                                                        <div class="row" data-id="{{ $button->id }}">
                                                            <div class="col-md-1 text-center sort-icon"><span class="glyphicon glyphicon-sort"></span></div>
                                                            <div class="col-md-3">{{ $button->text }}</div>
                                                            <div class="col-md-5">
                                                                @if($button->targetPage)
                                                                    {{ $button->targetPage->abstract }}
                                                                @else
                                                                    <span style="color: grey">（尚未設定）</span>
                                                                @endif
                                                                @if($button->isKeyButton)
                                                                    <span class="bg-success">（過關按鈕，目標頁面將被忽視）</span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-3">
                                                                {!! link_to_route('button.edit', '編輯', $button->id, ['class' => 'btn btn-default']) !!}
                                                                {!! Form::open(['route' => ['button.destroy', $button->id], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此按鈕嗎？');"]) !!}
                                                                {!! Form::submit('刪除', ['class' => 'btn btn-danger']) !!}
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    @empty
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                暫無按鈕
                                                            </div>
                                                        </div>
                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                暫無頁面
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        統計
                    </div>
                    <div class="panel-body">
                        @forelse($mission->inStatsPages as $page)
                            <div class="row">
                                <div class="col-md-8">
                                    <h5>{{ $page->abstract }}</h5>
                                </div>
                                <div class="col-md-4 text-right">
                                    最後更新：<span title="{{ (new Carbon($page->stats['time']))->diffForHumans() }}">{{ $page->stats['time'] }}</span>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($page->stats['stats'] as $key => $stats)
                                    <div class="col-md-3 text-right">
                                        {{ $stats['text'] }}
                                    </div>
                                    <div class="col-md-9">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $stats['percent'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $stats['percent'] }}%; min-width: 6em">
                                                {{ $stats['count'] }} ({{ $stats['percent'] }} %)
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <hr />
                        @empty
                            暫無頁面列入統計
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! HTML::script('//code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}
    {!! HTML::script('build-js/js.cookie.js') !!}
    <script>
        {{-- 排序頁面 --}}
        pageList = $("#accordion");
        pageList.sortable({
            handle: ".sort-icon",
            placeholder: {
                element: function (currentItem) {
                    return $("<div class=\"placeholder text-center\">拖曳排序</div>")[0];
                },
                update: function (container, p) {
                    return null;
                }
            },
            update: function( event, ui ) {
                pageList.sortable("disable");
                {{-- 取得任務ID --}}
                var missionId = {{ $mission->id }};
                console.log(missionId);
                {{-- 狀態文字 --}}
                var missionStatusText = $(".mission-status-text");
                missionStatusText.html("<span class=\"text-info\"><span class=\"fa fa-refresh fa-spin\"></span>儲存中...</span>");
                {{-- 新ID清單 --}}
                var idList = [];
                ui.item.parent().children("div").each(function () {
                    thisId = $(this).data("id");
                    idList.push(thisId);
                });
                console.log(idList);
                {{-- 透過ajax更新順序 --}}
                var URLs = "{{ URL::route('page.sort' ) }}";
                $.ajax({
                    url: URLs,
                    data: {
                        missionId: missionId,
                        idList: idList
                    },
                    headers: {
                        'X-CSRF-Token': "{{ Session::token() }}",
                        "Accept": "application/json"
                    },
                    type: "POST",
                    dataType: "text",

                    success: function (data) {
                        if (data == "success") {
                            missionStatusText.html("<span class=\"text-success\"><span class=\"fa fa-check\"></span>已儲存</span>");
                        } else {
                            missionStatusText.html("<span class=\"text-danger\"><span class=\"fa fa-times\"></span>發生錯誤：" + data + "</span>");
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        missionStatusText.html("<span class=\"text-danger\"><span class=\"fa fa-times\"></span>發生錯誤：" + xhr.status + " " + thrownError + "</span>");
                    },
                    complete: function(data) {
                        pageList.sortable("enable");
                    }
                });
            }
        });
        {{-- 排序按鈕 --}}
        buttonItems = $(".button-items");
        buttonItems.sortable({
            handle: ".sort-icon",
            placeholder: {
                element: function (currentItem) {
                    return $("<div class=\"placeholder text-center\">拖曳排序</div>")[0];
                },
                update: function (container, p) {
                    return null;
                }
            },
            update: function( event, ui ) {
                buttonItems.sortable("disable");
                {{-- 取得頁面ID --}}
                var pageId = ui.item.parent().data("page-id");
                {{-- 狀態文字 --}}
                var statusText = $(".status-text[data-page-id=" + pageId +"]");
                statusText.html("<span class=\"text-info\"><span class=\"fa fa-refresh fa-spin\"></span>儲存中...</span>");
                {{-- 新ID清單 --}}
                var idList = [];
                ui.item.parent().children("div").each(function () {
                    thisId = $(this).data("id");
                    idList.push(thisId);
                });
                {{-- 透過ajax更新順序 --}}
                var URLs = "{{ URL::route('button.sort' ) }}";
                $.ajax({
                    url: URLs,
                    data: {
                        pageId: pageId,
                        idList: idList
                    },
                    headers: {
                        'X-CSRF-Token': "{{ Session::token() }}",
                        "Accept": "application/json"
                    },
                    type: "POST",
                    dataType: "text",

                    success: function (data) {
                        if (data == "success") {
                            statusText.html("<span class=\"text-success\"><span class=\"fa fa-check\"></span>已儲存</span>");
                        } else {
                            statusText.html("<span class=\"text-danger\"><span class=\"fa fa-times\"></span>發生錯誤：" + data + "</span>");
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        statusText.html("<span class=\"text-danger\"><span class=\"fa fa-times\"></span>發生錯誤：" + xhr.status + " " + thrownError + "</span>");
                    },
                    complete: function(data) {
                        buttonItems.sortable("enable");
                    }
                });
            }
        });
        {{-- 記錄頁面展開狀態 --}}
        $(document).ready(function () {
            accordion = $("#accordion");
            //when a group is shown, save it as the active accordion group
            accordion.on('shown.bs.collapse', function () {
                var active = $("#accordion .in").attr('id');
                Cookies.set('activeAccordionGroup_{{ $mission->id }}', active);
                //  alert(active);
            });
            accordion.on('hidden.bs.collapse', function () {
                Cookies.remove('activeAccordionGroup_{{ $mission->id }}');
            });
            var last = Cookies.get('activeAccordionGroup_{{ $mission->id }}');
            if (last != null) {
                //remove default collapse settings
                $("#accordion .panel-collapse").removeClass('in');
                //show the account_last visible group
                $("#" + last).addClass("in");
            }
        });
    </script>
@endsection
