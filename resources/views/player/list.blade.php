@extends('app')

@section('title')
    玩家清單
@endsection

@section('css')
    {!! HTML::style('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css') !!}
    <style type="text/css">
        {{-- 使表格文字垂直置中 --}}
        .table > tbody > tr > td {
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        玩家清單
                        {!! link_to_route('player.export', '[匯出]') !!}
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-8">
                                {!! Form::open(['route' => 'player.index', 'class' => 'form-inline', 'method' => 'GET']) !!}
                                {!! Form::text('q', Input::get('q'), ['id' => 'q', 'placeholder' => '搜尋NID', 'class' => 'form-control', 'style' => 'width: 400px']) !!}
                                <div class="input-group" style="width: 200px;">
                                    任務完成數：<span id="amount"><span class="fa fa-refresh fa-spin"></span>Loading...</span>
                                    <div id="slider-range" style=" margin: 5px 20px 10px 20px"></div>
                                    {!! Form::hidden('min', null, ['id' => 'min']) !!}
                                    {!! Form::hidden('max', null, ['id' => 'max']) !!}
                                </div>
                                <div class="input-group">
                                    <button type="submit" class="btn btn-primary" title="搜尋"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    @if(Input::has('q') || Input::has('min') || Input::has('max'))
                                        <a class="btn btn-default" href="{{ URL::current() }}" title="清除搜尋結果"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    @endif
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4">
                                @if(Input::has('q') || Input::has('min') || Input::has('max'))
                                    @if($players->count())
                                        符合條件的資料共 {{ $totalCount }} 筆
                                    @else
                                        找不到符合條件的資料
                                    @endif
                                @else
                                    共 {{ \App\Player::count() }} 筆資料
                                @endif
                            </div>
                        </div>

                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">NID</th>
                                <th class="text-center">任務準時完成數<br />（括號為非必要任務）</th>
                                @foreach($missions as $mission)
                                    <th class="text-center" title="{{ $mission->title }}<br />共 {{ count($records[$mission->id]) }} 人完成">
                                        #{{ $mission->id }}
                                        @if($mission->require)
                                            <span class="fa fa-check text-success"></span>
                                        @endif
                                    </th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($players as $key => $player)
                                <tr>
                                    <td class="text-center">{{ (Input::get('page',1) - 1) * $amountPerPage + $key + 1}}</td>
                                    <td class="@if(!\App\Helper\StringHelper::isNID($player->nid)) danger @elseif(!$player->price_id && $player->canGetPrice) success @endif">
                                        {{ $player->nid }}
                                        @if($player->price_id)
                                            <span class="fa fa-check pull-right" title="禮物已領取<br />Time：{{ $player->get_price_at }}<br />Price：{{ $player->price->name }}" style="margin-left: 5px"></span>
                                        @elseif($player->canGetPrice)
                                            <span class="fa fa-gift pull-right" title="可領取禮物" style="margin-left: 5px"></span>
                                        @endif
                                        @if(!\App\Helper\StringHelper::isNID($player->nid))
                                            <span class="fa fa-exclamation-triangle pull-right" title="不符合NID格式" style="margin-left: 5px"></span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ $player->validRequireRecords->count() }}
                                        @if($player->validRequireRecords->count() < $player->validRecords->count())
                                            (+{{ $player->validRecords->count() - $player->validRequireRecords->count() }})
                                        @endif
                                    </td>
                                    @foreach($missions as $mission)
                                        @if(isset($records[$mission->id][$player->nid]))
                                            <td class="@if($records[$mission->id][$player->nid]['valid']) success @else info @endif text-center" title="Time：{{ $records[$mission->id][$player->nid]['time'] }}（{{ (new Carbon($records[$mission->id][$player->nid]['time']))->diffForHumans() }}）<br />IP：{{ $records[$mission->id][$player->nid]['ip'] }}">
                                                @if($records[$mission->id][$player->nid]['valid'])
                                                    <span class="fa fa-check text-success"></span>
                                                @else
                                                    <span class="fa fa-clock-o text-info"></span>
                                                @endif
                                            </td>
                                        @elseif($mission->require)
                                            <td class="danger text-center">
                                                <span class="fa fa-times text-danger"></span>
                                            </td>
                                        @else
                                            <td class="text-center">
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="{{ 3 + $missions->count() }}" class="text-center">
                                        無資料可顯示
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! str_replace('/?', '?', $players->appends(Input::except(['page']))->render()) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! HTML::script('//code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}
    <script>
        $('form').on('submit', function () {
            var $q = $("#q");
            var $min = $("#min");
            var $max = $("#max");

            if(!$q.val() && $min.val() == 0 && $max.val() == {{ $missions->count() }}){
                return false;
            }
            if(!$q.val()){
                $q.attr("disabled","disabled");
            }
            if($min.val() == 0){
                $min.attr("disabled","disabled");
            }
            if($max.val() == {{ $missions->count() }}){
                $max.attr("disabled","disabled");
            }
        });
        function updateRange(min, max) {
            var $min = $("#min");
            var $max = $("#max");
            $( "#amount" ).text(min + " ～ " + max);
            $min.val(min);
            $max.val(max);
        };
        $(function() {
            $sliderRange = $("#slider-range");
            $sliderRange.slider({
                range: true,
                min: 0,
                max: {{ $missions->count() }},
                values: [ {{ Input::has('min') ? Input::get('min') : 0 }}, {{ Input::has('max') ? Input::get('max') : $missions->count() }} ],
                slide: function( event, ui ) {
                    updateRange(ui.values[0], ui.values[1]);
                }
            });
            updateRange($sliderRange.slider("values", 0), $sliderRange.slider("values", 1));
        });
    </script>
@endsection
