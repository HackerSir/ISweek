@extends('app')

@section('title')@if(isset($button))編輯@else新增@endif按鈕@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(isset($button))
                            編輯按鈕
                        @else
                            新增按鈕
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @if(isset($button))
                                {!! Form::model($button, ['route' => ['button.update', $button->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                            @else
                                {!! Form::open(['route' => ['button.store'], 'class' => 'form-horizontal']) !!}
                            @endif
                            <div class="form-group">
                                {!! Form::label(null, '所屬任務', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    {!! link_to_route('mission.show', $page->mission->title, $page->mission->id, ['target' => '_blank']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label(null, '所屬頁面', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    {!! \App\Helper\StringHelper::nl2br($page->info) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('text'))?' has-error':'' }}">
                                {!! Form::label('text', '按鈕文字', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('text', null, ['required', 'placeholder' => '請輸入按鈕文字', 'class' => 'form-control']) !!}
                                    @if($errors->has('text'))
                                        <span class="label label-danger">{{ $errors->first('text') }}</span>
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('target_page_id'))?' has-error':'' }}">
                                {!! Form::label('target_page_id', '目標頁面', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::select('target_page_id', $page->mission->pageSelections, $page->mission->target_page_id, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('is_key_button'))?' has-error':'' }}">
                                {!! Form::label('is_key_button', '過關按鈕', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    <label>
                                        {!! Form::checkbox('is_key_button', true, null) !!}
                                        過關按鈕（將忽視上方目標頁面）
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-2">
                                    {!! Form::hidden('page_id', $page->id) !!}
                                    {!! Form::submit('更新按鈕', ['class' => 'btn btn-primary']) !!}
                                    {!! HTML::linkRoute('mission.show', '返回', $page->mission->id, ['class' => 'btn btn-default']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var isChecked;
        var targetPageSelect;
        $('input[name=is_key_button]').click(function (event) {
            isChecked = $(this).is(':checked');
            if(isChecked){
                targetPageSelect.prop('disabled', true);
            }else{
                targetPageSelect.prop('disabled', false);
            }
        });
        $('form').on('submit', function () {
            $('select[name=target_page_id]').prop('disabled', false);
        });
        $(document).ready(function (){
            isChecked = $('input[name=is_key_button]').is(':checked');
            targetPageSelect = $('select[name=target_page_id]');
            if(isChecked){
                targetPageSelect.prop('disabled', true);
            }
        });
    </script>
@endsection
