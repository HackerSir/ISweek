@extends('app')

@section('css')
    {!! HTML::style('css/typed.css') !!}
    <style type="text/css">
        #dialogPageButtonGroup > button {
            margin-right: 5px;
            margin-bottom: 5px;
        }

        .map-float {
            position: absolute;
            top: 0;
            left: 0;
        }

        .btn-circle {
            width: 100px;
            height: 100px;
            padding: 17px;
            font-size: 50px;
            line-height: 1.33;
            border-radius: 50%;
            border-width: 5px;
            border-color: gold;
        }

        .mission-title {
            color: white;
            font-size: 30px;
            pointer-events: none;
            /*防止使用者選取文字*/
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        #mission-carousel img {
            width: 90%;
        }

        .carousel-control {
            width: 5%;
        }

        .carousel-indicators {
            top: -30px;
            bottom: 0;
            left: 40%;
            width: 80%;
        }

        /* 複寫 Carousel 底下的點點樣式 */
        .carousel-indicators li {
            width: 24px;
            height: 24px;
            background-color: white;
            border-width: 1px;
            border-color: black;
            border-radius: 50%;
            opacity: 0.6;
        }

        .carousel-indicators .active {
            background-color: #2e6da4;
            width: 24px;
            height: 24px;
        }

        .progress-bar {
            min-width: 6em
        }

        .tooltip {
            position: fixed;
        }
    </style>
@endsection

@section('content')
    @if(Entrust::hasRole('admin') && Auth::user()->inDebugMode)
        <div class="alert alert-warning" style="float:left; position: fixed; left: 5px;">
            除錯開關：<span class="fa fa-check text-success" title="已開啟除錯模式<br />可於個人資料頁面切換"></span><br/>
            不受關卡限制 && 不會記錄「任務進度」
        </div>
    @endif
    <div class="container">
        <i id="loadingIcon" class="fa fa-spinner fa-spin fa-5x pull-right hidden" style="position: absolute; right: 20px; color: grey;"></i>
        <div class="row">
            <div id="startPage" class="col-sm-8 col-sm-offset-2 text-center animated">
                {!! HTML::image('img/HackerInMission.png', Config::get('site.name'), ['class' => 'img-responsive center-block', 'style' => 'width: 60%']) !!}
                <h4>逢甲資安週闖關活動</h4>

                <h3>{{ $startTime->format('Y-m-d') }} ~ {{ $endTime->format('Y-m-d') }}</h3>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                        <input type="text" id="username" name="username" class="form-control input-lg" placeholder="學號/教職員證號" autofocus autocomplete="on">
                        <button id="startButton" class="btn btn-primary btn-lg btn-block" style="margin-top: 1vh;" type="button" disabled>
                            Start&nbsp;<i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div id="dialogPage" class="col-md-8 col-md-offset-2 animated hidden" style="font-size: 18px;">
                <div style="min-height: 25px;">
                    <a id="showAllTextLink" style="cursor: pointer;">跳過動畫</a>
                    <a class="pull-right" data-target_page_id="-1" id="skipLink" style="cursor: pointer;">跳過</a>
                    <div class="clearfix"></div>
                </div>
                <div class="well well-lg">
                    <div id="textArea"></div>
                    <br/>
                    <span id="pushSpaceKeyTip" class="hidden-xs pull-right">請按空白鍵繼續<i class="fa fa-caret-down fa-lg fa-fw typed-cursor"></i></span>
                    <div class="clearfix" style="height: 25px;"></div>
                </div>
                <div id="dialogPageButtonGroup" class="text-center" style="min-height: 46px; margin-bottom: 10px;"></div>
            </div>

            <div id="carouselPage" class="col-md-12 hidden" style="margin-bottom: 10px;">
                <div id="mission-carousel" class="carousel slide" data-ride="carousel" data-interval="false" style="margin-top: 40px">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#mission-carousel" data-slide-to="0" class="active"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active"></div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#mission-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#mission-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div id="statsPage" class="col-md-12 hidden" style="margin-bottom: 10px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 style="margin-top: 0; margin-bottom: 0;">看看其他人的選擇</h3>
                    </div>
                    <div class="panel-body">
                        <blockquote id="statsInfo" style="white-space: pre-wrap"></blockquote>
                        <div class="row" style="margin: 0" id="statsButton"></div>
                        <p>更新時間：<span id="updateTime"></span></p>
                    </div>
                </div>
            </div>


            <div id="mapPage" class="col-md-12 animated hidden">
                <div class="page-header" style="margin: 0;">
                    <div class="btn-group btn-group-lg pull-right" style="margin-bottom: 5px;" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="viewOptions" id="viewMapOption" data-target="#viewMap" autocomplete="off" checked><i class="fa fa-map"></i>
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="viewOptions" id="viewListOption" data-target="#viewList" autocomplete="off"><i class="fa fa-list-ol"></i>
                        </label>
                    </div>
                    <h3 id="usernameTitle" style="margin: 0;"></h3>
                </div>
                <div class="clearfix"></div>
                <div class="tab-content" style="padding: 0">
                    <div id="viewMap" class="tab-pane active" style="padding: 0; position: relative; overflow: hidden; border: 5px solid grey; border-radius: 5px;">
                        <img id="mapImage" src="{{ env('MAP_IMAGE_URL', "") }}" alt="任務地圖" class="img-responsive"/>
                    </div>
                    <div id="viewList" class="tab-pane">
                        <div id="missionsList" class="list-group"></div>
                        <i class="fa fa-check-circle fa-lg fa-fw" style="color: green; margin-top: 4px;"></i>：已完成<br/>
                        <i class="fa fa-star fa-lg fa-fw" style="color: yellowgreen; margin-top: 4px;"></i>：必要任務
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Minify::javascript([
        '/js/typed.min.js',
        '/js/game.js',
        '/js/game-dialog.js',
        '/js/game-map.js',
        '/js/game-mission.js'
    ])->withFullUrl() !!}

    <script type="text/javascript">
        var Xtoken = "{{ Session::token() }}";
        var api_index = "{{ URL::route('api.index') }}";
        var api_mission = "{{ URL::route('api.mission') }}";
        var api_result = "{{ URL::route('api.result') }}";

        var isMissionStarted = false;

        window.onbeforeunload = function (e) {
            if (isMissionStarted) {
                return '任務尚未完成！！！';
            }
            return null;
        }
    </script>
@endsection
