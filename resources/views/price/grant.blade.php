@extends('app')

@section('title')獎品發放@endsection

@section('css')
    <style>
        #nid-box > input {
            font-size: 50px;
            height: auto;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        獎品發放
                        （<label title="學號輸入框自動取得焦點（建議開啟）">{!! Form::checkbox('autoFocus', true, true) !!} Auto Focus</label>）
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'price.grant', 'method' => 'get', 'id' => 'nid-box', 'class' => 'form-inline']) !!}
                        {!! Form::text('nid', null, ['class' => 'form-control','placeholder' => '請掃描學生證或輸入NID', 'autofocus', 'required']) !!}
                        {!! Form::submit('確認', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        玩家資料
                    </div>
                    <div class="panel-body" id="player-box">

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        發送獎品
                        <a href="javascript:void(0)" class="btn" id="priceUpdateButton"><span class="fa fa-refresh" id="priceUpdateIcon"></span> 更新</a>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-warning" role="alert">
                            <strong>請注意</strong>
                            <ul>
                                <li>若玩家未完成所有任務，將無法領獎</li>
                                <li>每位玩家限領獎一次</li>
                                <li>點擊獎項後立即記錄，且無法修改</li>
                            </ul>
                        </div>
                        <div id="price-box">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! HTML::script('build-js/js.cookie.js') !!}
    <script>
        var $nidBox;
        var nid = '';
        var playerData = null;
        var autoFocus = true;
        function loadPrice(){
            var $priceBox = $('#price-box');
            var $priceUpdateButton = $('#priceUpdateButton');
            var $priceUpdateIcon = $('#priceUpdateIcon');
            $priceUpdateButton.attr("disabled", true);
            $priceUpdateIcon.addClass("fa-spin");
            $priceBox.html("讀取中...");
            {{-- 取得資料--}}
            var URLs = "{{ route('price.ajax-price' ) }}";
            $.ajax({
                url: URLs,
                data: {},
                headers: {
                    'X-CSRF-Token': "{{ Session::token() }}",
                    "Accept": "application/json"
                },
                type: "POST",
                dataType: "json",

                success: function (data) {
                    if($.isEmptyObject(data)){
                        $priceBox.html("無獎品資料");
                        return;
                    }
                    var buttonList = '';
                    if(playerData && !playerData.canGetPrice) {
                        buttonList += '<div class="alert alert-danger">該玩家無領獎資格</div>';
                    }
                    $.each(data, function(key, value){
                        var grantButton = '<div class="row" style="margin-bottom: 5px"><div class="col-md-12">';
                        if(value.left > 0) {
                            if(playerData && playerData.canGetPrice) {
                                grantButton += '<a href="javascript:void(0)" class="btn btn-lg btn-primary grant-button" data-id=' + value.id + '>' + value.name + '</a>';
                            }else{
                                grantButton += '<a href="javascript:void(0)" class="btn btn-lg btn-primary disabled" data-id=' + value.id + '>' + value.name + '</a>';
                            }
                            grantButton += ' <span class="text-info">剩餘：' + value.left + ' / ' + value.amount +'</span>';
                        }else{
                            grantButton += '<a href="javascript:void(0)" class="btn btn-lg btn-default disabled">' + value.name + '</a>';
                            grantButton += ' <span class="text-danger">剩餘：' + value.left + ' / ' + value.amount +'</span>';
                        }
                        grantButton += '</div></div>';
                        buttonList += grantButton;
                    });
                    $priceBox.html(buttonList);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $priceBox.html("<span class=\"text-danger\"><span class=\"fa fa-times\"></span>發生錯誤：" + xhr.status + " " + thrownError + "</span>");
                },
                complete: function(data) {
                    $priceUpdateButton.attr("disabled", false);
                    $priceUpdateIcon.removeClass("fa-spin");
                }
            });
        }
        function loadPlayer(playerNid){
            var $playerBox = $('#player-box');
            $playerBox.html('讀取中...');
            var $priceBox = $('#price-box');
            $priceBox.html("等待玩家資料載入");
            playerData = null;
            {{-- 取得資料--}}
            var URLs = "{{ route('price.ajax-player' ) }}";
            $.ajax({
                url: URLs,
                data: {
                    nid:playerNid
                },
                headers: {
                    'X-CSRF-Token': "{{ Session::token() }}",
                    "Accept": "application/json"
                },
                type: "POST",
                dataType: "json",

                success: function (data) {
                    if($.isEmptyObject(data)){
                        $playerBox.html(playerNid + " - 無玩家資料");
                        return;
                    }
                    playerData = data;
                    playerDataText = '<div style="font-size: 20px">';
                    playerDataText += '<div class="row">';
                    playerDataText += '<div class="col-md-4 text-right">NID</div><div class="col-md-8">' + playerData.nid + '</div>';
                    playerDataText += '</div><div class="row">';
                    playerDataText += '<div class="col-md-4 text-right">準時完成任務數</div><div class="col-md-8">' + playerData.finishMissionCount + ' / {{ \App\Mission::where('require', '=', true)->count() }}</div>';
                    playerDataText += '</div><div class="row">';
                    playerDataText += '<div class="col-md-4 text-right">完成所有任務</div><div class="col-md-8">' + ((playerData.finishAllMissions)?'O':'X') + '</div>';
                    playerDataText += '</div><div class="row">';
                    playerDataText += '<div class="col-md-4 text-right">領獎時間</div><div class="col-md-8">' + ((playerData.getPriceAt)?playerData.getPriceAt:'-') + '</div>';
                    playerDataText += '</div><div class="row">';
                    playerDataText += '<div class="col-md-4 text-right">領獎獎項</div><div class="col-md-8">' + ((playerData.getPriceAt)?playerData.getPriceName:'-') + '</div>';
                    playerDataText += '</div><div class="row">';
                    playerDataText += '<div class="col-md-4 text-right">可領獎？</div><div class="col-md-8">' + ((playerData.canGetPrice)?'O':'X') + '</div>';
                    playerDataText += '</div>';
                    playerDataText += '</div>';
                    $playerBox.html(playerDataText);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $playerBox.html("<span class=\"text-danger\"><span class=\"fa fa-times\"></span>發生錯誤：" + xhr.status + " " + thrownError + "</span>");
                },
                complete: function(data) {
                    loadPrice();
                }
            });
        }
        function sendPrice(priceId){
            var URLs = "{{ route('price.ajax-grant' ) }}";
            $.ajax({
                url: URLs,
                data: {
                    nid: playerData.nid,
                    price_id: priceId
                },
                headers: {
                    'X-CSRF-Token': "{{ Session::token() }}",
                    "Accept": "application/json"
                },
                type: "POST",
                dataType: "json",

                success: function (data) {
                    if(data.success){
                        show_message('success',"成功發送獎品：" + data.price);
                    } else {
                        show_message('error',"發生錯誤：" + data.error);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    show_message('error',"發生錯誤：" + xhr.status + " " + thrownError);
                },
                complete: function(data) {
                    loadPlayer(nid);
                }
            });
        }
        $('#nid-box').submit(function(event){
            nid = $nidBox.val();
            loadPlayer(nid);
            $nidBox.val('');
            $nidBox.focus();
            return false;
        });
        $('#priceUpdateButton').click(function(){
            loadPrice();
        });
        $('input[name=autoFocus]').click(function () {
            var isChecked = $(this).is(':checked');
            Cookies.set('autoFocus', isChecked);
            autoFocus = isChecked;
        });
        $(document).on( 'click', '.grant-button', function (event) {
            if(nid.length < 1){
                show_message('error', '未輸入學號');
                return;
            }
            if(playerData == null){
                show_message('error', '非有效玩家');
                return;
            }
            if(!playerData.canGetPrice){
                show_message('error', '該玩家無法領獎');
                return;
            }
            var confirmResult = confirm('確定要發送 ' + $(event.target).text() + ' 嗎？');
            if(!confirmResult){
                return;
            }
            sendPrice($(this).data('id'));
        });
        $(document).on( 'click', '*', function (event) {
            if(!autoFocus){
                return;
            }
            $nidBox.focus();
        });
        $(document).ready(function(){
            $nidBox = $('input[name=nid]');
            //AutoFocus
            if(typeof Cookies.get('autoFocus') !== 'undefined' && Cookies.get('autoFocus') == 'false'){
                autoFocus = false;
                $('input[name=autoFocus]').attr('checked', false);
            }
            loadPrice();
        });
    </script>
@endsection
