@extends('app')

@section('title')獎品管理@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        獎品管理
                    </div>
                    <div class="panel-body">
                        <div class="pull-right">
                            {!! link_to_route('price.grant', '獎品發放', [], ['class' => 'btn btn-primary']) !!}
                            {!! link_to_route('price.create', '新增獎品', [], ['class' => 'btn btn-primary']) !!}
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>獎品名稱</th>
                                    <th>總數量</th>
                                    <th>送出數量</th>
                                    <th>剩餘數量</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($prices as $price)
                                    <tr>
                                        <td>{{ $price->name }}</td>
                                        <td>{{ $price->amount }}</td>
                                        <td>{{ $price->players->count() }}</td>
                                        <td>{{ $price->left }}</td>
                                        <td>
                                            {!! link_to_route('price.edit', '編輯', $price->id, ['class' => 'btn btn-default']) !!}
                                            {!! Form::open(['route' => ['price.destroy', $price->id], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此獎品嗎？');"]) !!}
                                            {!! Form::submit('刪除', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            暫無獎品
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
