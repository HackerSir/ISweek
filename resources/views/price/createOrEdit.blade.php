@extends('app')

@section('title')@if(isset($price))編輯@else新增@endif獎品@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(isset($price))
                            編輯獎品
                        @else
                            新增獎品
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @if(isset($price))
                                {!! Form::model($price, ['route' => ['price.update', $price->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                            @else
                                {!! Form::open(['route' => ['price.store'], 'class' => 'form-horizontal']) !!}
                            @endif
                            <div class="form-group has-feedback{{ ($errors->has('name'))?' has-error':'' }}">
                                {!! Form::label('name', '獎品名稱', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('name', null, ['required', 'placeholder' => '請輸入獎品名稱', 'class' => 'form-control']) !!}
                                    @if($errors->has('name'))
                                        <span class="label label-danger">{{ $errors->first('name') }}</span>
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('amount'))?' has-error':'' }}">
                                {!! Form::label('amount', '獎品數量', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::number('amount', null, ['required', 'placeholder' => '請輸入獎品數量', 'class' => 'form-control', 'min' => 0, 'step' => 1]) !!}
                                    @if($errors->has('amount'))
                                        <span class="label label-danger">{{ $errors->first('amount') }}</span>
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-2">
                                    {!! Form::submit('更新獎品', ['class' => 'btn btn-primary']) !!}
                                    {!! HTML::linkRoute('price.index', '返回', [], ['class' => 'btn btn-default']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
