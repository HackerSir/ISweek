@extends('app')

@section('css')
    {!! HTML::style('//cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.css') !!}
    <style type="text/css">
        #countDown {
            font-family: "Agency FB", "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        .countDownClock {
            font-family: inherit;
            width: 620px;
            margin: 1em auto;
        }
    </style>
@endsection

@section('content')
    <div class="container text-center" id="countDown">
        <h1>{{ Config::get('site.name') }}</h1>
        <h4>逢甲資安週闖關活動</h4>
        <br/>
        <div class="countDownClock hidden-xs"></div>
        <br/>
        <h2>Coming Soon ...</h2>
        <h2>Start At {{ $startTime->format('Y-m-d') }}</h2>
    </div>
@endsection

@section('js')
    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.js') !!}
    <script>
        //要提前計算，否則可能會產生一次跳兩秒的現象
        var diff = parseInt(({{ $startTime->timestamp * 1000 }} - new Date().getTime()) / 1000);

        var clock = $('.countDownClock').FlipClock(diff, {
            clockFace: 'DailyCounter',
            countdown: true,
            callbacks: {
                //當倒數結束，自動重整
                stop: function() {
                    //但不要直接重整，要稍等一下，馬上重整不會到新畫面
                    setTimeout(function(){
                        window.location.reload(1);
                    }, 1500);
                }
            }
        });
    </script>
@endsection
