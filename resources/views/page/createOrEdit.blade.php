@extends('app')

@section('title')@if(isset($page))編輯@else新增@endif頁面@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(isset($page))
                            編輯頁面
                        @else
                            新增頁面
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @if(isset($page))
                                {!! Form::model($page, ['route' => ['page.update', $page->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                            @else
                                {!! Form::open(['route' => ['page.store'], 'class' => 'form-horizontal']) !!}
                            @endif
                            <div class="form-group">
                                {!! Form::label(null, '所屬任務', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    {!! link_to_route('mission.show', $mission->title, $mission->id, ['target' => '_blank']) !!}
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('info'))?' has-error':'' }}">
                                {!! Form::label('info', '頁面內容', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::textarea('info', null, ['required', 'placeholder' => '請輸入頁面內容', 'class' => 'form-control']) !!}
                                    @if($errors->has('info'))
                                        <span class="label label-danger">{{ $errors->first('info') }}</span>
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('images'))?' has-error':'' }}">
                                {!! Form::label('images', '圖片清單', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    <div class="form-control-static">上傳至{!! link_to('http://imgur.com/', 'Imgur', ['target' => '_blank'])  !!}</div>
                                    {!! Form::textarea('images', null, ['placeholder' => '請輸入圖片網址（每行一個網址）', 'class' => 'form-control']) !!}
                                    @if($errors->has('images'))
                                        <span class="label label-danger">{{ $errors->first('images') }}</span>
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('in_stats'))?' has-error':'' }}">
                                {!! Form::label('in_stats', '列入統計', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    <label>
                                        {!! Form::checkbox('in_stats', true, null) !!}
                                        列入統計（將在統計頁面顯示各按鈕選擇比例）
                                    </label>
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('typewriter'))?' has-error':'' }}">
                                {!! Form::label('typewriter', '打字機效果', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9 form-control-static">
                                    <label>
                                        @if(isset($page))
                                            {!! Form::checkbox('typewriter', true, null) !!}
                                        @else
                                            {!! Form::checkbox('typewriter', true, true) !!}
                                        @endif
                                        打字機效果（頁面文字逐字元顯示）
                                    </label>
                                </div>
                            </div>
                            <div class="form-group has-feedback{{ ($errors->has('stats_page_id'))?' has-error':'' }}">
                                {!! Form::label('stats_page_id', '顯示統計', ['class' => 'control-label col-md-2']) !!}
                                <div class="col-md-9">
                                    {!! Form::select('stats_page_id', $mission->pageSelections, $mission->stats_page_id, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-2">
                                    {!! Form::hidden('mission_id', $mission->id) !!}
                                    {!! Form::submit('更新頁面', ['class' => 'btn btn-primary']) !!}
                                    {!! HTML::linkRoute('mission.show', '返回', $mission->id, ['class' => 'btn btn-default']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
