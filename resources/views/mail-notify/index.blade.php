@extends('app')

@section('title')
    郵件通知
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">郵件通知</div>
                    <div class="panel-body text-center">
                        <div class="alert alert-danger">
                            <strong>警告：</strong>點擊將發送通知郵件給所有玩家<br />
                            目前玩家總數為：{{ \App\Player::count() }}
                        </div>
                        @if(env('ALLOW_MAIL_NOTIFY', false))
                            {!! Form::checkbox('confirm', true) !!} ← 勾選以啟動按鈕
                            {!! Form::open(['route' => 'mail-notify.send']) !!}
                            {!! Form::submit('發送', ['class' => 'btn btn-danger btn-lg', 'disabled' => 'disabled']) !!}
                            {!! Form::close() !!}
                        @else
                            請至環境設定檔修改<code>ALLOW_MAIL_NOTIFY</code>開啟此功能
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var submit = $('input[type=submit]');
        $('form').on('submit', function () {
            var result = confirm('警告：即將發送大量郵件，確定要發送嗎？');
            $('input[name=confirm]').attr('checked', false);
            submit.prop('disabled', true);
            return result;
        });
        $('input[name=confirm]').click(function (event) {
            var isChecked = $(this).is(':checked');
            if (isChecked) {
                submit.prop('disabled', false);
            } else {
                submit.prop('disabled', true);
            }
        });
    </script>
@endsection
