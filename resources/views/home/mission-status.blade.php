@extends('app')

@section('css')
    <style type="text/css">
        @if (env('BACKGROUND_IMAGE_URL'))
            body {
                background: url("{{ env('BACKGROUND_IMAGE_URL') }}") fixed;
                background-size: cover;
            }
        @endif
    </style>
    <style>
        .chartWithOverlay {
            position: relative;
        }

        .overlay {
            position: absolute;
            top: 50%; /* chartArea top  */
            left: 50%; /* chartArea left */
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h1>前線戰況</h1>
        <div class="well" style="opacity: 0.8">
            <div id="mission-status-chart" style="height: 500px"></div>
            <p class="lead">¹新勇者：今天剛成為勇者的人。<br/>²可選任務，不一定要完成。</p>
        </div>
        <h1>任務統計</h1>
        <div class="well" style="opacity: 0.8">
            @foreach($missionStats as $i => $missionStat)
                <div class="row">
                    <div class="col-md-12">
                        <h3>{{ $missionStat['title'] }}</h3>
                    </div>
                </div>
                @if($missionStat['isStart'])
                    @foreach($missionStat['pages'] as $j => $pageStat)
                        <div class="row">
                            <div class="col-md-6">
                                <blockquote>{!! $pageStat['info'] !!}</blockquote>
                                <p>最後更新時間：{!! $pageStat['time'] !!}</p>
                            </div>
                            <div class="col-md-6 chartWithOverlay">
                                <div id="mission-stats-chart-{{ $i }}-{{ $j }}" style="height: 300px"></div>
                                @if($pageStat['isNoData'])
                                    <div class="overlay">
                                        <div style="font-size: 64px;">沒有資料</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <blockquote><strong style="color: red;">任務尚未開放</strong></blockquote>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawMissionStatus);
        google.setOnLoadCallback(drawMissionStats);

        function drawMissionStatus() {
            var data = new google.visualization.DataTable({!! $missionDataTable->toJson() !!});

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2,
                {
                    calc: "stringify",
                    sourceColumn: 2,
                    type: "string",
                    role: "annotation"
                }]);

            var options = {
                title: "任務完成人數",
                bar: {groupWidth: "80%"},
                isStacked: true,
                chartArea: {
                    left: '25%',
                    width: '60%'
                },
                hAxis: {
                    title: '人數',
                    viewWindowMode: 'maximized'
                }
            };
            var chart = new google.visualization.BarChart(document.getElementById("mission-status-chart"));
            chart.draw(view, options);
        }

        function drawMissionStats() {
            @foreach($missionStats as $i => $missionStat)
                @foreach($missionStat['pages'] as $j => $pageStat)
                    drawMissionStat(
                        new google.visualization.DataTable({!! $pageStat['stats']->toJson() !!}),
                        'mission-stats-chart-{{ $i }}-{{ $j }}'
                    );
                @endforeach
            @endforeach
        }

        function drawMissionStat(data, divName) {
            var formatter = new google.visualization.NumberFormat(
                {pattern: '# 人'});
            formatter.format(data, 1);

            var options = {
                'chartArea': {'width': '100%', 'height': '80%'},
                sliceVisibilityThreshold: 0
            };

            var chart = new google.visualization.PieChart(document.getElementById(divName));
            chart.draw(data, options);
        }

        $(function () {
            $(window).resize(function () {
                drawMissionStatus();
                drawMissionStats();
            });
        });
    </script>

@endsection
