@extends('app')

@section('css')
    <style>
        h2 {
            font-size: 2.1em;
        }
        h3 {
            font-size: 1.5em;
        }
        p.text, li.text {
            font-size: 1.2em;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h1>工作人員列表</h1>
        <p class="text">感謝以下為資安週付出的人及單位！</p>
        <h2>資安週籌備單位</h2>
        <ul>
            <li class="text">主辦單位：逢甲大學資訊處、逢甲大學資通安全學程</li>
            <li class="text">協辦單位：逢甲大學黑客社、逢甲大學資訊安全策進會</li>
            <li class="text">活動負責人：李榮三　副教授</li>
            <li class="text">總召：魏國瑞</li>
            <li class="text">副召：李坤印、侯均靜</li>
            <li class="text">器材：李治軒、林冠佑</li>
            <li class="text">採購：康雅涵、陳瑜晴</li>
            <li class="text">機動：何修廷</li>
        </ul>
        <h3>綿羊牆Live Demo</h3>
        <ul>
            <li class="text">策劃：陳俊佑</li>
            <li class="text">解說：張辰德</li>
            <li class="text">網站：許展源</li>
        </ul>
        <h3>駭客出任務</h3>
        <ul>
            <li class="text">策劃：侯均靜</li>
            <li class="text">文案：侯均靜、荊輔翔</li>
            <li class="text">前端：陳靖德</li>
            <li class="text">後端：許展源</li>
            <li class="text">美工：林倚婕</li>
            <li class="text">宣傳：荊輔翔</li>
            <li class="text">特別感謝 <a href="http://wevote.tw/" target="_blank">沃草！立委出任務</a> 提供設計靈感</li>
        </ul>
    </div>
@endsection
