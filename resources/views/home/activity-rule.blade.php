@extends('app')

@section('css')
    <style type="text/css">
        h2 {
            font-size: 2.1em;
        }

        p.text, ol.text-list > li {
            font-size: 1.3em;
            margin-left: 2em;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h1>活動辦法</h1>
        <p style="font-size: 1.3em;">逢甲大學資安週是這幾年大力推動的活動之一，為了推廣資訊安全不遺餘力，今年也不免俗的舉辦了各式的活動，「駭客出任務」便是其中一環，希望每位學生可以透過這個網站，了解到各項資安議題。</p>
        <br>
        <h2>活動時程</h2>
        <p class="text">各位同學請注意時間喔！<span style="color: red">逾時不候</span>！</p>

        <p class="text">
            遊戲開放：<span title="{{ Carbon::create(2015,12,18,0,0,0)->diffForHumans() }}">2015年12月18日 凌晨0點</span><br />
            計分時間：<span title="{{ Carbon::create(2015,12,18,0,0,0)->diffForHumans() }}">2015年12月18日 凌晨0點</span> ～ <span title="{{ Carbon::create(2015,12,23,16,0,0)->diffForHumans() }}">2015年12月23日 <span style="color: red">下午4點</span></span> <br />
            抽獎時間：<span title="{{ Carbon::create(2015,12,23,11,40,0)->diffForHumans() }}">2015年12月23日 上午11點40分</span> ～ <span title="{{ Carbon::create(2015,12,23,16,30,0)->diffForHumans() }}">下午4點30分</span><br />
        </p>
        <h2>參加資格</h2>
        <p class="text">逢甲大學的學生及教職員工均有資格參加。</p>
        <h2>活動報名</h2>
        <p class="text">來參加就對了，沒有報名表喔！</p>
        <h2>活動流程</h2>
        <ol class="text-list">
            <li>玩家需在計分時間內完成指定的任務。</li>
            <li>完成後，請於12月23日抽獎時間攜帶學生證（或教職員證）至攤位抽獎<span style="color: red; font-weight: bold">（獎品有限，送完為止）</span>。
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="thumbnail">
                            {!! HTML::image('img/ISCMap.jpg', '攤位位置', ['class' => 'img-rounded']) !!}
                            <div class="caption">
                                <h3>攤位位置：忠勤樓前（與工學院的轉角處）</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>中大獎機率高達1/3，包含下列大獎可供選擇，即使沒中大獎，依然可以帶走我們準備的聖誕小禮物。<br />
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            {!! HTML::image('img/price2.jpg', '16G隨身碟', ['class' => 'img-rounded']) !!}
                            <div class="caption">
                                <h3>16G隨身碟</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            {!! HTML::image('img/price1.jpg', '瑰珀翠護手霜', ['class' => 'img-rounded']) !!}
                            <div class="caption">
                                <h3>瑰珀翠護手霜</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            {!! HTML::image('img/price3.jpg', '準備中', ['class' => 'img-rounded']) !!}
                            <div class="caption">
                                <h3>HP 電源轉接頭</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ol>
        <h2>注意事項</h2>
        <ol class="text-list">
            <li>抽獎時，請攜帶學生證（或教職員證）以供檢驗身份，證件上必須有可以識別之姓名、學號（或證號）及條碼。</li>
            <li>填寫資料時請填寫正確的，填錯後果自行負責。</li>
            <li>即便計分時間過了，網站依然不會關閉，請各位自行注意計分截止時間。</li>
            <li>如果網站因任何不可抗力因素影響遊戲，將不予以延長活動計分時間。</li>
        </ol>
    </div>
@endsection
