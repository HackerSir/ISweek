@extends('app')

@section('css')
    <style>
        h2 {
            font-size: 2.1em;
        }
        h3 {
            font-size: 1.8em;
        }
        p.text, li.text {
            font-size: 1.3em;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <ul class="nav nav-tabs">
            <li role="presentation" @if($tab == 'privacy') class="active" @endif>
                {!! HTML::linkRoute('privacy', '隱私權') !!}
            </li>
            <li role="presentation" @if($tab == 'terms') class="active" @endif>
                {!! HTML::linkRoute('terms', '服務條款') !!}
            </li>
            <li role="presentation" @if($tab == 'FAQ') class="active" @endif>
                {!! HTML::linkRoute('FAQ', '常見問題') !!}
            </li>
        </ul>
        <div class="panel panel-default">
            <div class="panel-body">
                @if($tab == 'privacy')
                    @include('home.privacy')
                @elseif($tab == 'terms')
                    @include('home.terms')
                @elseif($tab == 'FAQ')
                    @include('home.FAQ')
                @endif
            </div>
        </div>
    </div>
@endsection
